<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagination_helper
{
	protected $ci;
	public $res;

	public function __construct()
	{
        $this->ci =& get_instance();
	}
	public function config($total_data, $per_page, $path_url){
		if (!empty($_GET['page'])) {
	    	$offset = $_GET['page'];
	    }else{
	    	$offset = 0;
	    }	
	    $per_page = $per_page;

	    $tot = $total_data;
	    
	    $num_page = ceil($tot/$per_page);
	    $path_url = $path_url;
	    $page = $offset;
	    $data['tot'] = $tot;
	    $this->res = '';
        if ($page != 0) {
            $this->res .='
            <li>
                <a href="'.base_url($path_url).'?page='.($page-$per_page).'" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a>
            </li>
            ';
        }
        $num = 0;
        for ($i=0; $i < $num_page ; $i++) { 
            
            
            $display_num = $i+1;

            if ($i == 0) {
                if (empty($page)) {
                    $this->res .= '<li class="active"><a style="color:#ef9e10">'.$display_num.'</a> </li>';
                }else{
                    $this->res .= '<li><a href="'.base_url($path_url).'">'.$display_num.'</a> </li>';
                }
            }else{
                $num +=$per_page;
                if ($num == $page) {
                    $active = "active";
                    $this->res .= '<li class="active"><a style="color:#ef9e10;">'.$display_num.'</a> </li>';
                }else{
                    $active = '';
                    $this->res .= '<li><a href="'.base_url($path_url).'?page='.$num.'">'.$display_num.'</a> </li>';
                }
            }
            
        }
        if ($page != $num) {
            $this->res .= '
            <li>
                <a href="'.base_url($path_url).'?page='.($page+$per_page).'" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a>
            </li>
            ';
        }
	}
	public function render(){
		return $this->res;
	}

	

}

/* End of file Pagination_helper.php */
/* Location: ./application/libraries/Pagination_helper.php */
