<?php defined('BASEPATH') OR exit('No direct script access allowed');

class OutputView {

	protected $CI;

	function __construct()
    {
    	$this->CI =& get_instance();
    }

    //OUTPUT ADMIN PAGE
	public function auth()
    {
     	if (!$this->CI->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
    }

    function data_output($data_add)
	{
		$data = $data_add;
		$data['settings'] = $this->CI->db->get('settings', 1)->row();
		$data['title'] = $data['settings']->judul;

		return $data;
	}
    public function output_admin($view, $template, $data_add = null, $output = null)
	{
		$this->auth();
		$data = $this->data_output($data_add);
		if ($output) {
			$data['page'] = $this->CI->load->view($view, $output, TRUE); //GROCERY CRUD PAGE
		}else{
			$view = explode('<script', $this->CI->load->view($view, $data, TRUE));
			$data['page'] = $view[0]; //NON GROCERY CRUD
			if (isset($view[1])) {
				$data['scriptView'] = '<script'.$view[1];
			}
		}
		$this->CI->load->view('backend/'.$template, $data);
	}

    //OUTPUT FRONT PAGE
    public function output_front($view, $template, $data_add = null, $output = null)
	{
		$data['settings'] = $this->CI->db->get('settings', 1)->row();
		$data['title']    = $data['settings']->judul;
		if ($output) {
			$data['page'] = $this->CI->load->view($view, $output, TRUE); //GROCERY CRUD PAGE
		}else{
			$view = explode('<script', $this->CI->load->view($view, $data_add, TRUE));
			$data['page'] =  $view[0]; //NON GROCERY CRUD
			if (isset($view[1])) {
				$data['scriptView'] = '<script'.$view[1];
			}
		}
		$this->CI->load->view('template/'.$template, $data);
	}
}