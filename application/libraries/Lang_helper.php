<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lang_helper 
{
	protected $ci;
	protected $global_lang;

	public function __construct()
	{
        $this->ci =& get_instance();

        $this->global_lang = $this->ci->session->userdata('lang');
	}
	public function setLang(){
		return $this->var_lang = array(
			// All page 
			"search_placeholder" => "Ketik untuk mencari produk",
			"en_search_placeholder" => "Type to searching products",
			// homepage
			"title_product"		=> "Produk Kami",
			"en_title_product"	=> "Product Us",
			"title_news"		=> "Berita Dan Artikel",
			"en_title_news"		=> "News And Article",
			// page master division
			"home_division"		=> "Beranda",
			"en_home_division"	=> "Home",
			"menu_catalogue"	=> "Katalog",
			"en_menu_catalogue"	=> "Catalogue",
			"menu_product"		=> "Produk",
			"en_menu_product"	=>	"Product",
			"menu_contact"		=> "Kontak Kami",
			"en_menu_contact"	=> "Contact Us",
 			"title_page"		=> "Divisi",
			"en_title_page"		=> "Division",
			"title_division_category"	=> "Kategori",
			"en_title_division_category"=> "Categories",
			"title_division_feature"	=> "Produk Unggulan",
			"en_title_division_feature"	=> "Featuring Product",
			// footer
			"footer_about"		=> "Tentang PT Inter Delta",
			"en_footer_about"	=> "ABOUT PT INTER DELTA",
			"footer_link"		=> "Link Cepat",
			"en_footer_link"	=> "Quick Links",
			"footer_article"	=> "Artikel Terbaru",
			"en_footer_article"	=> "Latest Article",
			// product support
			"title_product_support"		=> "Dukungan Produk",
			"en_title_product_support"	=> "Product Supports"

			
		);
	}

	

}

/* End of file Lang_helper.php */
/* Location: ./application/libraries/Lang_helper.php */
