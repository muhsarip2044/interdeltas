<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_helper
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}
	public function get_html_header($division=null)
	{
		$res = '
		<header id="header" class="header-transparent header-fullwidth dark" style="background: #ffb901">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="'.base_url().'" class="logo" data-dark-logo="'.base_url('assets/frontend/images/'.$this->ci->component->logo_white).'">
                            <img src="'.base_url('assets/frontend/images/'.$this->ci->component->logo).'" alt="'.$this->ci->component->company.'">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Top Search Form-->
                    <div id="top-search">
                        <form action="'.base_url("search").'" method="post">
                            <input type="text" name="keyword" class="form-control" value="" placeholder="'.$this->ci->var_lang[$this->ci->global_lang."search_placeholder"].'">
                        </form>
                    </div>
                    <!--end: Top Search Form--> 

                    <!--Header Extras-->
                    <div class="header-extras">
                        <ul>
                            <li>
                                <!--top search-->
                                <a id="top-search-trigger" href="#" class="toggle-item">
                                    <i class="fa fa-search"></i>
                                    <i class="fa fa-close"></i>
                                </a>
                                <!--end: top search-->
                            </li>
                            <li>
                                <div class="topbar-dropdown">
                                    <a class="title"><i class="fa fa-globe"></i></a>
                                    <div class="dropdown-list">
                                        <a href="'.base_url("home/change_language/0").'" class="list-entry" href="#">Bahasa</a>
                                        <a href="'.base_url("home/change_language/en_").'" class="list-entry" href="#">English</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--end: Header Extras-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li>
                                        <a href="'.base_url().'">'.$this->ci->var_lang[$this->ci->global_lang."home_division"].'</a>
                                    </li>
                                    <li class="dropdown">
                                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$this->ci->var_lang[$this->ci->global_lang."menu_catalogue"].' <span class="caret"></span></a>
                                      ';

                                      	if (count($division->catalogue)>0) {
                                            $res .= '<ul class="dropdown-menu">';
	                                        foreach ($division->catalogue as $key) {
	                                            $res .= '
	                                            <li><a href="'.base_url($key->url_prefix).'">'.$key->name.'</a></li>
	                                            ';
	                                        }
                                            $res .= '</ul>';
	                                    }

                                    $res .=  '
                                    </li>
                                    <li class="dropdown">
                                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$this->ci->var_lang[$this->ci->global_lang."menu_product"].' <span class="caret"></span></a>
                                      ';

                                        if (count($division->category)>0) {
                                            $res .= '<ul class="dropdown-menu">';
                                            foreach ($division->category as $key) {
                                                $res .= '
                                                <li><a href="'.base_url($key->url_prefix).'">'.$key->name.'</a></li>
                                                ';
                                            }
                                            $res .= '</ul>';
                                        }
                                     
                                    $res .=   '
                                    </li>
                                    <li>
                                        <a href="'.base_url("kontak-kami/".$division->slug).'">'.$this->ci->var_lang[$this->ci->global_lang."menu_contact"].'</a>
                                    </li>                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
		';
		return $res;
	}

	

}

/* End of file Division_helper.php */
/* Location: ./application/libraries/Division_helper.php */
