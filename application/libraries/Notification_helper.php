<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_helper
{
	protected $ci;
	protected $cms;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->get_config();
	}
	public function get_config(){
		$this->ci->load->model('setting');
		$this->cms = Setting::where("id_settings",1)->first();
		return true;
	}

	public function build()
	{	
		$config['protocol'] = 'smtp';
	    $config['smtp_host'] = 'ssl://smtp.gmail.com';
	    $config['smtp_port'] = '465';
	    $config['smtp_user'] = $this->cms->email_system;//'kodefresh.dev@gmail.com';  //change it
	    $config['smtp_pass'] = $this->cms->password_email_system;//'saripstar277'; //change it
	    $config['charset'] = 'utf-8';
	    $config['newline'] = "\r\n";
	    $config['mailtype'] = 'html';
	    $config['wordwrap'] = TRUE;
		return $config;
	}
	public function forward_message($visitor_email=null,$visitor_name=null,$subject=null,$message=null,$division_id=null){
		// validasi id divisi
		$this->ci->load->model('division');

		$division = Division::where("id",$division_id)->first();

		if (count($division)==0) {
			// Mengirim pesan gagal kirim

			return false;
		}

		// get config email

		// meneruskan pesan ke email divisi
		$this->ci->load->library('email');
		    
	    $this->ci->email->initialize($this->build());

	    $this->ci->email->from($this->cms->email_system);   //change it
	    $this->ci->email->to($division->email);       //change it
	    $this->ci->email->subject($this->cms->nama_perusahaan." : Forward Surat Visitor Website.");
	    $this->ci->email->message('
	    	<div>
	    		Hi, admin '.$division->name.',<br>
	    		Pengunjung website '.$this->cms->nama_perusahaan.' mengirimi Pesan pada divisi anda, berikut ini adalah detail pesannya : <br>
	    		Nama Pengirim : '.$visitor_name.' <br>
	    		Email : '.$visitor_email.'<br>
	    		Judul Surat : '.$subject.'<br>
	    		Isi Surat : <p>'.$message.'<p><br><br>
	    		<br>
	    		<p style="color:silver">Perhatian untuk tidak membalas email ini. Email ini dikirim oleh sistem website.</p>
	    		<br>
	    		<br>
	    		<br>
	    		<p style="color:silver">'.$this->cms->footer.'</p>
	    	</div>

	    ');
	    if ($this->ci->email->send()) {
	    	return true;
	    } else {
	    	return $this->ci->email->print_debugger();
	    }
	}

	

}

/* End of file Notification_helper.php */
/* Location: ./application/libraries/Notification_helper.php */
