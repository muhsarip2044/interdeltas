<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
  public $global_lang;
  public $var_lang;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here

    // set language
    if (empty($this->session->userdata('lang'))) {
      $array = array(
        'lang' => ''
      );
      
      $this->session->set_userdata( $array );
    }
    
    // Inisiasi library lang
    $this->load->library('lang_helper');
    $this->global_lang = $this->session->userdata('lang');
    $this->var_lang = $this->lang_helper->setLang();
  }
	public function index()
	{
		 
	}
  public function getCaptcha(){
    //captcha system
    $this->load->helper('captcha');
    $vals = array(
            'word'          => $this->incrementalHash(7),
            'img_path'      => './captcha/',
            'img_url'       => base_url('captcha/'),
            //'font_path'     => './path/to/fonts/texb.ttf',
            'img_width'     => '250',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size'     => 20,
            'img_id'        => 'Imageid',
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                    'background' => array(255, 255, 255),
                    'border' => array(255, 255, 255),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 40, 40)
            )
    );

    $cap = create_captcha($vals);
    $array = array(
      'captcha_word' => $cap['word']
    );
    
    $this->session->set_userdata( $array );
    return $cap;
  }
  private function incrementalHash($length = 7){
    $random_string="";
    while(strlen($random_string)<$length && $length > 0) {
            $randnum = mt_rand(0,61);
            $random_string .= ($randnum < 10) ?
                    chr($randnum+48) : ($randnum < 36 ? 
                            chr($randnum+55) : $randnum+61);
     }
    return $random_string;
  }
  public function toAscii($str, $replace=array(), $delimiter='-') {
    if( !empty($replace) ) {
      $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
  }
  

}
 
 /**
  * summary
  */
class CLIENT_Controller extends MY_Controller
{
  /**
  * summary
  */
  public $component;
  public $menu_frontend;
  public $social_media;
  public $quick_link;
  public $last_article;

  public $folder_view_frontend = "frontend/";
  public function __construct()
  {
    parent::__construct();

    // set component website
    $this->load->model('component');
    $this->component = Component::find(1);

    // get quick link for footer
    $this->load->model('menu');
    $this->load->model('division');
    $this->quick_link = Menu::select("id","parent_id","name","division_id","type","slug")->where("parent_id",null)->whereDoesntHave("children")->with(['division' => function($q){
      $q->select("id",'slug', 'name');
    }])->orderBy("sort","asc")->get();

    // get social media component
    $this->load->model('social_media');
    $this->social_media = Social_media::where("status",1)->get();

    // get last article, batas 3
    $this->load->model('blog');
    $this->load->model('blog_category');
    $this->last_article = Blog::where("status",1)
      ->select("id","slug","category_id","name","created_at")
      ->with("category")
      ->limit(3)
      ->orderBy("created_at","desc")
      ->get();





    if ($this->session->userdata('visitor') == null) {
      $array = array(
        'visitor' => 1
      );
      $ip = $this->input->ip_address();
      $this->session->set_userdata( $array );
      $object = array('dates' => date("Y-m-d",time()), 'qty'=>1, 'ip'=>$ip );
      $this->db->insert('tbl_client_visitor', $object);
    }    
  }

  // Get menu homepage

  public function getMenuHomepage(){
    $this->load->model('menu');
    $this->load->model('division');
    return Menu::where("parent_id",null)
        ->select(
            "id",
            $this->global_lang."name",
            "parent_id",
            "division_id",
            "type",
            "status",
            "slug",
            "sort"
        )
        ->orderBy("sort","asc")
        ->with(['parent' => function($q){
        $q->select("id","parent_id",'slug', $this->global_lang."name");
    }])
    ->with(['children' => function($q){
        $q->select("id","parent_id",'slug', $this->global_lang."name");
        }])
    ->with(['division' => function($q){
        $q->select("id",'slug', 'name');
    }])
    ->get(); 
  }

  public function get_header_division($slug)
  {
    // Mengambil data division
    $this->load->model('division');
    $this->load->model('category_product');
    $this->load->model('product_feature');
    $this->load->model('product');
    $this->load->model('catalogue');
    $division = Division::where("slug",$slug)
      ->with(["category","feature","catalogue"])
      ->first();

    if (count($division)==0) {
      // slug divisi tidak valid, kembalikan halaman 404

      show_404();
      die();
    }

    // load library untuk generate html
    $this->load->library('division_helper');
    return array("html"=>$this->division_helper->get_html_header($division), "data"=>$division);
  }
 }
 /**
  * summary
  */
 class ADMIN_Controller extends MY_Controller
 {
     /**
      * summary
      */
     public $user;
     public $userGroup = array();
     public $cms;
     public $ADMIN_NAMESPACE = "interadmin";
     public $resize_size_image;
     public $resize_maintain_ratio=true;
     public $resize_min_size;
     public $resize_max_size;



     public function __construct()
     {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
          redirect('auth');
        }
        $this->cms = $this->db->where("id_settings",1)->get("settings"); 


        $this->user = $this->ion_auth->user()->row();
        $this->db->where('user_id', $this->user->id);
        $groups    = $this->db->get('users_groups',1);
        if ($groups->num_rows() > 0) {
          $this->userGroup = $groups->row()->group_id;
        }else{
          $this->ion_auth->logout();
          redirect('auth');  
        }
        
      }
     /*
      * Merubah string menjadi url friendly
     */
     public function callback_slug($post_array) {
      $post_array['slug'] = $this->toAscii($post_array['name']);
       
      return $post_array;
    } 

    /*
    * Meresize gampar yang telah di upload
    */
    public function callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
      $sizes = $this->resize_size_image;

      $this->load->library('image_lib');
      for($i=0;$i<count($sizes);$i++){
        $config['image_library']    = 'gd2';
        $config['source_image']     = $field_info->upload_path.'/'.$uploader_response[0]->name;;
        $config['new_image']     = $field_info->upload_path.'/'.$sizes[$i][2].$uploader_response[0]->name;
        $config['maintain_ratio']   = $this->resize_maintain_ratio;
        $config['width']            = $sizes[$i][1];
        $config['height']           = $sizes[$i][0];   

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
      }
      return true;
    }
    public function callback_before_upload($files_to_upload,$field_info)
    {
    /*
    * Examples of what the $files_to_upload and $field_info will be:    
    $files_to_upload = Array
    (
    [sd1e6fec1] => Array
    (
    [name] => 86.jpg
    [type] => image/jpeg
    [tmp_name] => C:\wamp\tmp\phpFC42.tmp
    [error] => 0
    [size] => 258177
    )

    )

    $field_info = stdClass Object
    (
    [field_name] => file_url
    [upload_path] => assets/uploads/files
    [encrypted_field_name] => sd1e6fec1
    )

    */
      // cek minimal size


      foreach ($files_to_upload as $key) { 
        if ($key['size'] < $this->resize_min_size) {
          return 'File yang anda upload terlalu kecil, silahkan upload ulang dengan gambar ukuran lebih dari '.($this->resize_min_size/1000)."kb";  
        }

        // cek maximal size
        if ($key['size'] > $this->resize_max_size) {
          return 'File tidak bisa du upload karena melebihi dari .'.($this->resize_max_size/1000)."kb";  
        }        
      }


      

      // cek direktori
      if(is_dir($field_info->upload_path))
      {
        return true;
      }
      else
      {
        return 'Folder penyimpanan gambar tidak valid, hubungi pengembang untuk mengeceknya.';    
      }

    }
 }

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */