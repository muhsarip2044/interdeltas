<?php
// application/core/MY_Exceptions.php
class MY_Exceptions extends CI_Exceptions {
	public function __construct()
    {
        parent::__construct();
    }

    function show_404($page = '', $log_error = TRUE)
    {

        $CI =& get_instance();

        	// init menu
	    $CI->load->model('menu');
        $CI->load->model('division');
        $foo = Menu::where("parent_id",null)
            ->select("id","name","parent_id","division_id",'type',"status","slug","sort")
            ->orderBy("sort","asc")
            ->with(['parent' => function($q){
            $q->select("id","parent_id",'slug', 'name');
        }])
        ->with(['children' => function($q){
            $q->select("id","parent_id",'slug', 'name');
            }])
        ->with(['division' => function($q){
            $q->select("id",'slug', 'name');
        }])
        ->get();
        
        $data['menu_homepage'] = json_decode($foo) ;

        $data['title']  = "Halaman yang anda tuju tidak ditemukan.";
		$data['content']  = $CI->load->view('frontend/page/my404', $data, true);
		$CI->load->view('frontend/master_blog',$data);

        //$CI->load->view('404_read_view');
        echo $CI->output->get_output();
        exit;
    }
}