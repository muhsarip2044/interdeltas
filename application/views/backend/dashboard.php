<div class="row">
  <div class="col-lg-12">
    <!-- Default box -->
    <div class="box box-solid bg-teal-gradient">
      <div class="box-header">
        <i class="fa fa-th"></i>

        <h3 class="box-title">Pengunjung Website</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
          </button>
        </div>
      </div>
      <div class="box-body border-radius-none">
        <div class="chart" id="line-chart" style="height: 250px;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
<!-- Small boxes (Stat box) -->
<!-- Dashboard untuk admin -->
<div class="row " >
  <section class="content-header text-center">    
    <h2>
      <ol class="breadcrumb">
        <li class="active">Pengaturan Utama</li>
      </ol>
    </h2>
    <pre><?php echo $this->userGroup ?></pre>
  </section>
  <?php 
  if (count($widget)>0) { 
    $single_widget = "";
    $multiple_widget = "";
    

    foreach ($widget as $key ) {
      // genrate random colour
      $colors = array('green','yellow','red','light-blue','aqua');
      $number_random = array_rand($colors);
      $bg_color = $colors[$number_random];
      if ($key->level_one == 0 && $key->url != "#") {
        $single_widget .= '
        <div class="col-lg-3 col-xs-6 text-center">
          <a href="'.base_url($this->ADMIN_NAMESPACE.'/'.$key->url).'">
            <div class="small-box bg-'.$bg_color.'">
              <div class="inner">
                <h3><i class="fa fa-'.$key->icon.'"></i></h3>

                <h4 >'.$key->label.'</h4>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </a>
        </div>
        ';
      }else{
        if ($key->url == "#") {
          $multiple_widget .= '
          <section class="content-header col-lg-12 col-xs-12 text-center">    
            <h2>
              <ol class="breadcrumb">
                <li class="active">'.$key->label.'</li>
              </ol>
            </h2>
          </section>
          ';
          if (count($key->children)>0) {
            foreach ($key->children as $keys) {
              $multiple_widget .= '
              <div class="col-lg-3 col-xs-6 text-center">
                <a href="'.base_url($this->ADMIN_NAMESPACE.'/'.$keys->url).'">
                  <div class="small-box bg-'.$bg_color.'">
                    <div class="inner">
                      <h3><i class="fa fa-'.$keys->icon.'"></i></h3>

                      <h4 >'.$keys->label.'</h4>
                    </div>
                    <div class="icon">
                      <i class="ion ion-bag"></i>
                    </div>
                  </div>
                </a>
              </div>
              ';
            }
          }
        }
      }
    }
    echo $single_widget.$multiple_widget;
  }
  ?>
</div>
<!-- /.row -->

  <script type="text/javascript">
  $(document).ready(function(){
      var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: <?php echo $visitor; ?>,
      xkey: 'period',
      ykeys: ['qty'],
      labels: ['Visitor'],
      lineColors: ['#efefef'],
      lineWidth: 2,
      hideHover: 'auto',
      gridTextColor: "#fff",
      gridStrokeWidth: 0.4,
      pointSize: 4,
      pointStrokeColors: ["#efefef"],
      gridLineColor: "#efefef",
      gridTextFamily: "Open Sans",
      gridTextSize: 10
    });

  });
  </script>