<div class="row">
	<div class="col-lg-12">
		<h3>
			Silahkan pilih divisi terlebih dahulu :
		</h3>
		<div class="list-group">
			<?php 
			if ($division->num_rows()>0) {
				foreach ($division->result() as $key) {
					echo '
						<a href="'.base_url($this->ADMIN_NAMESPACE."/products/index/add/?division_id=".$key->id).'" class="list-group-item">'.$key->name.'</a>
					';
				}
			}
			?>
		</div>
	</div>
</div>