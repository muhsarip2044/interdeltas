
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="keyword" content="Codeigniter, bootstrap, Grocerycrud">
  <meta name="description" content="Custom Framework Codeigniter and bootstrap">
  <meta name="author" content="Asrul Hanafi">
  <title><?php echo $title ?></title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/backend/img/'.$this->cms->row()->favicon) ?>">
 
  <!--GroceryCRUD CSS-->
  <?php if (isset($css_files)) : ?>
      <?php foreach($css_files as $file): ?>
          <link rel="stylesheet" href="<?php echo $file; ?>" />
      <?php endforeach; ?>
  <?php endif ?> 
  <!--Bootstrap-->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/bootstrap.min.css') ?>">
  <!--Font-->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/font-awesome.min.css') ?>">
  <!--AdminLTE-->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/skins/_all-skins.min.css">
  <!--Alertify-->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/alertify.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/default.min.css') ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/js/plugins/morris/morris.css') ?>">
  
  <link href="<?php echo base_url('assets/backend/css/pace/dataurl.css') ?>" rel="stylesheet" />
  <!--CSS PLUGINS-->
  <?php if (isset($css_plugins)): ?>
      <?php foreach ($css_plugins as $url_plugin): ?>
          <link rel="stylesheet" href="<?php echo base_url("$url_plugin") ?>">
      <?php endforeach ?>
  <?php endif ?>
  <!-- GroceryCRUD JS -->
  <?php if (isset($js_files)) { foreach($js_files as $file): ?> 
      <script src="<?php echo $file; ?>"></script>
  <?php endforeach; } else { ?>
      <script src="<?php echo base_url('assets/backend/js/plugins/jQuery/jQuery-2.1.4.min.js') ?>"></script>             
  <?php } ?>       
  <!--JS Plugins-->
  <?php if (isset($js_plugins)): ?>
      <?php foreach ($js_plugins as $url_plugin): ?>
          <script src="<?php echo base_url($url_plugin) ?>"></script>                
      <?php endforeach ?>
  <?php endif ?>
  <!--Custom CSS-->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/a-design.css') ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="sidebar-mini wysihtml5-supported fixed layout-boxed skin-green-light sidebar-collapse">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url("interadmin") ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $this->cms->row()->judul ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?php echo $this->cms->row()->judul ?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><?php echo $this->cms->row()->nama_perusahaan  ?></a>
      </div>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li>
            <a href="<?php echo base_url(); ?>" target="_blank">
              <i class="fa fa-chevron-right"></i> Lihat Website
            </a>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/backend/img/'.$this->user->image) ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->user->first_name." ".$this->user->last_name  ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/backend/img/'.$this->user->image) ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->user->first_name." ".$this->user->last_name  ?>
                  
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url($this->ADMIN_NAMESPACE.'/crud/account/edit/'.$this->ion_auth->get_user_id()) ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo  base_url('auth/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/backend/img/'.$this->user->image) ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info" style="margin-top: 0px !important;">
          <p><?php echo $this->user->first_name." ".$this->user->last_name  ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu list" id="menuSub">
        <?php 
        $headerMenu = $this->db
            ->Select("header_menu.*")
            ->join("groups_header","groups_header.id_header_menu = header_menu.id_header_menu")
            ->where("groups_header.id_groups in (".$this->userGroup.")")
            ->group_by("header_menu.id_header_menu")
            ->order_by("sort","asc")
            ->get("header_menu");
            
        if ($headerMenu->num_rows()>0) {
            foreach ($headerMenu->result() as $key) {
                echo '
                <li class="header">'.$key->header.'</li>
                ';
                $submenu1 = $this->db
                    ->select("menu.*")
                    ->where("groups_menu.id_groups in (".$this->userGroup.")")
                    ->where("level_one",0)
                    ->where("id_header_menu",$key->id_header_menu)
                    ->join("groups_menu","groups_menu.id_menu = menu.id_menu")
                    ->group_by("menu.id_menu")
                    ->order_by("menu.sort","asc")
                    ->get("menu");
                    //echo $this->db->last_query(); die();
                if ($submenu1->num_rows()>0) {

                    foreach ($submenu1->result() as $key2 ) {
                        $submenu2 = $this->db 
                            ->select("menu.*")
                            ->where("groups_menu.id_groups in (".$this->userGroup.")")
                            ->where("level_one",$key2->id_menu)
                            ->join("groups_menu","groups_menu.id_menu = menu.id_menu")
                            ->group_by("menu.id_menu")
                            ->order_by("menu.sort","asc")
                            ->get("menu");
                        if ($submenu2->num_rows()>0) {
                            echo '
                                <li class="treeview">
                                    <a href="'.base_url($this->ADMIN_NAMESPACE."/".$key2->url).'">
                                        <i class="fa fa-'.$key2->icon.'"></i> <span>'.$key2->label.'</span> <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                            ';
                            foreach ($submenu2->result() as $key3) {
                                echo '
                                    <li id="">
                                        <a href="'.base_url($this->ADMIN_NAMESPACE."/".$key3->url).'" class="name"><i class="fa fa-'.$key3->icon.'" class="name"></i> '.$key3->label.'</a>
                                    </li>
                                ';
                            }
                            echo '
                                    </ul>
                                </li>
                            ';
                        }else{
                            echo '
                                <li id="">
                                    <a href="'.base_url($this->ADMIN_NAMESPACE."/".$key2->url).'" class="name"><i class="fa fa-'.$key2->icon.'"></i> <span>'.$key2->label.'</span></a>
                                </li>
                            ';
                        }

                    }
                }
            }
        }
        ?>
       
    </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <ol class="breadcrumb">
            <?php if (!isset($crumb)){ ?>
                <li class="active">
                    <i class="fa fa-dashboard"></i> Dashboard
                </li>            
            <?php }else{ ?>           
                <?php foreach ($crumb as $label => $link): ?>
                    <?php if ($link == ''){ ?>
                        <li class="active">
                                <?php echo $label ?>
                            </li>
                    
                    <?php }else{ ?>
                        <li>
                            <a href="<?php echo site_url($link) ?>"> <?php echo $label ?></a>
                        </li>            
                    <?php } ?>
                <?php endforeach ?>
            <?php } ?>
          </ol>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content exspan-bottom">
            <?php echo $page ?>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <?php echo $this->cms->row()->footer ?>
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url('assets/backend/js/plugins/morris/morris.min.js') ?>"></script>
<!-- pace js -->
<script src="<?php echo base_url('assets/backend/js/plugins/pace/pace.js') ?>"></script>
<!-- froala -->

<!--Bootstrap JS-->
<script src="<?php echo base_url('assets/backend/js/bootstrap.min.js') ?>"></script>
<!--Alertify JS-->
<script src="<?php echo base_url('assets/backend/js/alertify.min.js') ?>"></script>
<!--AdminLTE JS-->
<script src="<?php echo base_url('assets/backend/js/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/plugins/fastclick/fastclick.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/js/app.min.js') ?>"></script>
<script>
    site         = '<?php echo site_url(); ?>';
    ur_class     = '<?php echo $this->uri->segment(1); ?>';
    url_function = '<?php echo $this->uri->segment(2); ?>';
    <?php echo isset($script) ? $script : '' ?>
    function datatablesOptions() { var option = { "orderClasses": false, <?php echo isset($data['script_datatables']) ? $data['script_datatables'] : ''  ?> }; return option; }
    function afterDatatables() { <?php echo isset($data['script_grocery']) ? $data['script_grocery']: '' ?> }

    
</script>
<script src="<?php echo base_url('assets/backend/js/list.min.js') ?>"></script>
<?php echo isset($scriptView) ? $scriptView : ''; ?>
<!--Custom JS-->
<script src="<?php echo base_url('assets/backend/js/a-design.js') ?>"></script>
</body>
</html>
