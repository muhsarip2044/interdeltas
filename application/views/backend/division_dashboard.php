<!-- Small boxes (Stat box) -->
<!-- Dashboard untuk admin -->
<div class="row " >
  <section class="content-header text-center">    
    <h2>
      <ol class="breadcrumb">
        <li class="active">Menu Pengaturan Division</li>
      </ol>
    </h2>
  </section>
  <?php 
  if (count($widget)>0) { 
    $single_widget = "";
    $multiple_widget = "";
    

    foreach ($widget as $key ) {
      // genrate random colour
      $colors = array('green','yellow','red','light-blue','aqua');
      $number_random = array_rand($colors);
      $bg_color = $colors[$number_random];
      $single_widget .= '
        <div class="col-lg-3 col-xs-6 text-center">
          <a href="'.base_url($this->ADMIN_NAMESPACE.'/'.$key->url).'">
            <div class="small-box bg-'.$bg_color.'">
              <div class="inner">
                <h3><i class="fa fa-'.$key->icon.'"></i></h3>

                <h4 >'.$key->label.'</h4>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </a>
        </div>
        ';
    }
    echo $single_widget.$multiple_widget;
  }
  ?>
</div>
<!-- /.row -->

  <script type="text/javascript">
  $(document).ready(function(){
      var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: <?php echo $visitor; ?>,
      xkey: 'period',
      ykeys: ['qty'],
      labels: ['Visitor'],
      lineColors: ['#efefef'],
      lineWidth: 2,
      hideHover: 'auto',
      gridTextColor: "#fff",
      gridStrokeWidth: 0.4,
      pointSize: 4,
      pointStrokeColors: ["#efefef"],
      gridLineColor: "#efefef",
      gridTextFamily: "Open Sans",
      gridTextSize: 10
    });

  });
  </script>