<!-- Shop products -->
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-md-9">
                <!-- Page title -->
                <div class="page-title" >
                    <h4>
                        Tag <strong><i><?php echo $tag; ?></i></strong> <br>
                        <small>Menampilkan <?php echo count($products) ?> produk.</small>
                    </h4>
                </div>
                <!-- end: Page title -->
                <!--Product list-->
                <div class="shop">
                    <div class="grid-layout grid-3-columns" data-item="grid-item">
                    <?php 
                    if (count($products)>0) {
                        foreach ($products as $key) {
                            echo '
                    <div class="grid-item">
                        <div class="product">
                            <div class="product-image">
                                <a style="width:265px;height:210px;" href="'.base_url($key->url_prefix).'"><img alt="Shop product image!" src="'.base_url($key->image_path.$key->image).'">
                                </a>
                                <div class="product-overlay">
                                    <a href="'.base_url($key->url_prefix).'" >Detail</a>
                                </div>
                            </div>

                            <div class="product-description">
                                <div class="product-category">'.$key->category->name.'</div>
                                <div class="product-title">
                                    <h3><a href="'.base_url($key->url_prefix).'">'.$key->name.'</a></h3>
                                </div>
                                <p>
                                '.$key->description.'
                                </p>
                            </div>
                        </div>
                    </div>
                            ';
                        }
                    }else{
                        echo '
                        <br>
                        <h5 style="color:gray">Hasil tidak ditemukan.</h5>
                        ';
                    }
                    ?>
                    </div>
                    
                    <hr>
                </div>
            </div>
            <!-- end: Content-->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="widget clearfix widget-tags">
                    <h4 class="widget-title">Tag Lainnya</h4>
                    <div class="tags">
                        <?php 
                        if (count($tags)>0) {
                            foreach ($tags as $key) {
                                echo '
                                <a href="'.base_url($key->url_prefix).'">'.$key->name.'</a>
                                ';
                            }
                        }
                        ?>
                    </div>
                </div>


            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Shop products -->

