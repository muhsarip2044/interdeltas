
<!-- 404 PAGE -->
<section class="m-t-80 p-b-150">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="page-error-404">404</div>
			</div>
			<div class="col-md-6">
				<div class="text-left">
					<h1 class="text-medium">Maaf! Halaman yang anda tuju tidak ditemukan.</h1>
					<p class="lead">Mungkin halaman telah berganti link atau telah di hapus dari website.</p>
					<div class="seperator m-t-20 m-b-20"></div>

					<div class="search-form">
						<p>Silahkan coba kembali</p>
                                    
                        <form action="<?php echo base_url("search") ?>" method="post" class="form-inline">
							<div class="input-group input-group-lg">
								<input type="text" name="keyword" class="form-control" value="" placeholder="Search for pages...">
                                <span class="input-group-btn">
		                            		<button class="btn color btn-primary" type="submit"><i class="fa fa-search"></i></button>									
                        		</span>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<!-- end:  404 PAGE -->
