<link href="<?php echo base_url() ?>assets/frontend/css/select2.min.css" rel="stylesheet">
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-md-9">
                <div class="row m-b-20"> 
                    <div class="breadcrumb fancy m-l-15">
                        <ul>
                            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i></a> </li>
                            <li><a href="<?php echo $url_division; ?>">Division : <?php echo $division->name ?></a> </li>
                            <li class="active"><a href="#">Category : <?php echo $cat->name ?></a> </li>
                        </ul>
                    </div>
                    <?php if($brands->num_rows()>0 or count($options) >0){ ?>
                    <div class="row" style="padding:15px;">
                      
                        <div class="col-lg-12">
                            <form class="form-inline" id="frmFilter" >
                                <div class="form-group" style="margin-right:10px;width:auto;">
                                    <?php 
                                    if($brands->num_rows()>0){
                                        echo '<label for="email">Brand / Merk </label>';
                                            echo '<select name="brand" id="selectBrand" multiple="multiple" class="form-control">';
                                            foreach ($brands->result() as $key) {
                                                echo '<option value="'.$key->id.'">'.$key->name.'</option>';
                                            }
                                            echo '</select>';
                                        }
                                    ?>
                                </div>
                                <?php 
                                if (count($options)>0) {
                                    foreach ($options as $key ) {
                                        echo '<div class="form-group"  style="margin-right:10px;width:auto;">';

                                        if(count($key->spec)>0){
                                            echo '<label>'.$key->option.'  </label>';
                                            echo '<select name="spec[]" multiple="multiple" class="form-control selectSpec" >';
                                            foreach ($key->spec as $key2) {
                                                echo '<option value="'.$key2->id.'">'.$key2->name.'</option>';
                                            }
                                            echo '</select>';
                                        }


                                        echo '</div>';
                                    }
                                }
                                ?>
                                <div class="form-group" style="margin-right:10px;padding-top:30px;">
                                    <button id="btnFilterClear" class="btn btn-warning"><i class="fa  fa-lg fa-times-circle"></i></button>
                                    <button id="btnFilterApply" type="submit" class="btn btn-default"><i class="fa  fa-lg fa-check-square"></i></button>
                                </div>
                                
                            </form>
                        </div>
                        
                    </div>
                    <?php } ?>
                    
                </div>
                <!--Product list-->
                <div class="shop">
                    <div id="product-area" class="grid-layout row" data-item="grid-item">
                    </div>
                    <div class="grid-layout grid-1-columns text-center">
                        <button id="btnLoadMore" class="btn btn-primary">Load More</button>
                    </div>
                    
                    
                </div>
                <!--End: Product list-->
            </div>
            <!-- end: Content-->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <!--widget newsletter-->
                <div class="widget clearfix widget-archive">
                    <h4 class="widget-title">Other categories</h4>
                    <ul class="list list-lines">
                        <?php 
                        if (count($division->category)>0) {
                            foreach ($division->category as $key) {
                                echo '
                                <li><a href="'.base_url($key->url_prefix).'" >'.$key->name.'</a></li>
                                ';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <!-- Brand -->
                <div class="widget clearfix widget-archive">
                    <?php 
                    if($brands->num_rows()>0){
                        echo '
                        <h4 class="widget-title">Brands</h4>
                            <ul class="list list-lines">
                        ';
                        foreach ($brands->result() as $key) {
                            echo '
                            <li><a href="'.base_url().'" >'.$key->name.'</a></li>
                            ';
                        }
                        echo '
                            </ul>
                        ';
                    }
                    ?>
                </div>
                <!-- end of brand -->
                <div class="widget clearfix widget-tags">
                    <h4 class="widget-title">Tags</h4>
                    <div class="tags">
                        <?php 
                        if (count($tags)>0) {
                            foreach ($tags as $key) {
                                echo '
                                <a href="'.base_url($key->url_prefix).'">'.$key->name.'</a>
                                ';
                            }
                        }
                        ?>
                    </div>
                </div>


            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<script type="text/javascript" src="<?php echo base_url() ?>assets/frontend/js/select2.min.js"></script>
<script>
    // Init multiple select filter
    $('#selectBrand').select2({
        placeholder: 'Select the brand',
        allowClear: true
    });

    $('.selectSpec').select2({
        placeholder: 'Select here',
    });

    // Event click clear filter
    $(".select2-container").css("width","auto");
    $(".select2-container").css("min-width","130px");


    // Fill data product
    var limit = <?php  echo $limit; ?>;
    var base_url = "<?php echo base_url() ?>";
    var category_id = <?php echo $category_id; ?>;
    var offset = 0;
    var url_get = base_url + 'categories/ajax_get_product/' + category_id + "/" + offset;
    var brand_ready = "0";
    var spec_ready = "0";

    getData(0);

    
    function getData(){
        $("#btnLoadMore").html("<i class='fa fa-spinner fa-spin fa-fw'></i> Loading Data ....");
        $("#btnLoadMore").attr("disabled",true);
        setTimeout(function(){ 
            $.getJSON( base_url + 'categories/ajax_get_product/' + category_id + "/" + offset + "/" + brand_ready + "/" + spec_ready, function(result){
                var numItem = result.length;
                if(numItem > 0){
                    for(var i = 0; i < numItem; i++){
                        var elem = setTemplate(result[i]);
                        $("#product-area").append(elem);
                    }
                    if (numItem < 5) {
                        // Terjadi bukan pada page pertama, menandakan record sudah pada posisi terakhir
                        $("#btnLoadMore").html("<i class='fa fa-info-circle'></i>This is the last product page.");
                        $("#btnLoadMore").removeClass("btn-primary");
                        $("#btnLoadMore").addClass("btn-warning");
                    }else{
                        // Terjadi karena result masih ada untuk di load berikutnya
                        $("#btnLoadMore").html("Load More");
                        $("#btnLoadMore").removeAttr("disabled"); 
                    }
                    


                }else{
                    $("#btnLoadMore").removeClass("btn-primary");
                    $("#btnLoadMore").addClass("btn-warning");  
                    if (offset == 0) {
                        // Terjadi jika page pertama tidak ada hasil
                        $("#btnLoadMore").html("<i class='fa fa-info-circle'></i>There is no product match with this filter.");
                    }else{
                        // Terjadi bukan pada page pertama, menandakan record sudah pada posisi terakhir
                        $("#btnLoadMore").html("<i class='fa fa-info-circle'></i>This is the last product page.");
                        
                    }

                    
                }
            });
        }, 1000);
        
    }

    function setTemplate(data){
        var url_product = base_url + 'products/' + data.division + '/' + data.slug;

        return '<div class="col-lg-4">'+
            '<div class="product">'+
                '<div class="product-image">'+
                    '<a href="' + url_product + '">'+
                        '<img style="width:265px;height:210px;" alt="Printer 2" src="' + base_url + 'assets/frontend/images/products/medium_' + data.image + '">'+
                    '</a>'+
                    '<div class="product-overlay">'+
                        '<a href="' + url_product + '">Detail</a>'+
                    '</div>'+
                '</div>'+
                '<div class="product-description">'+
                    '<div class="product-category">' + data.category + '</div>'+
                    '<div class="product-title">'+
                        '<h3><a href="' + url_product + '">' + data.name + '</a></h3>'+
                    '</div>'+
                    '<p>'+
                    '</p><p>' + data.description + '</p>'+
                    '<p></p>'+
                '</div>'+
            '</div>'+
        '</div>';
    }
    $("#btnFilterClear").on("click",function(e){
        e.preventDefault();
        $("#selectBrand").val(null).trigger("change");
        $(".selectSpec").val(null).trigger("change");
        offset=0;
        brand_ready = "0";
        spec_ready = "0";
        $("#product-area").html("");
        getData();
    }); 

    // Event load more
    $("#btnLoadMore").on("click",function(e){
        e.preventDefault();
        
        offset =offset+limit;

        getData();

    });

    // Event click filter
    $("#btnFilterApply").on("click",function(e){
        e.preventDefault();
        var data = $("#frmFilter").serialize();
        //window.location.href =  "<?php echo $url ?>?" + data;

        // Mendapatkan array
        var brands = $("#selectBrand").val();
        var brand_string = [];
        
        if (brands != null) {
            for(var i = 0; i < brands.length; i++){
                brand_string.push(brands[i]);
            }
            brand_ready = brand_string.join("-");
        };
        
        // Mendapatkan array spec
        spec_ready = $('select[name^=spec]').map(function(idx, elem) {
            return $(elem).val();
        }).get().join("-");
      
        $("#product-area").html("");
        offset = 0;
        
        getData();
        

    });   
</script>