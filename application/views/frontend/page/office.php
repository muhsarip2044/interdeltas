<!-- Page title -->
<section id="page-title" data-parallax-image="<?php echo base_url("assets/frontend/images/bg-contact.jpg") ?>">
	<div class="container">
		<div class="page-title">
			<h1>Offices</h1>
			<span></span>
		</div>
		<div class="breadcrumb">
					<ul>
				<li><a href="<?php echo base_url() ?>">Beranda</a>
				</li>
				<li><a href="#">Offices</a>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- end: Page title -->
<!-- CONTENT -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-uppercase">Head Office</h3>
                <address>
              <strong><?php echo $this->component->company ?></strong><br>
              <?php echo $this->component->address ?> <br>
              <abbr title="Phone">Phone:</abbr> <?php echo $this->component->phone ?>
              </address>
            </div>
            <div class="col-md-6">
                <h3 class="text-uppercase">Branch Office</h3>
                <div class="row">
                	<?php 
                	if (count($office)>0) {
                		foreach ($office as $key) {
                			echo '
							<div class="col-md-6">
								<h4><strong>'.$key->region.'</strong></h4>
		                        <address>
		                          '.$key->address.' <br>
		                          <abbr title="Phone">Phone:</abbr> '.$key->phone.' <br>
		                          <abbr title="Phone">Fax:</abbr> '.$key->fax.'
		                          </address>
		                    </div>
                			';
                		}
                	}
                	?>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- end: CONTENT -->

