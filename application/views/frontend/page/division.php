<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="360">
    <?php 
    if (count($slide)>0) {
        foreach ($slide as $key ) {
            $url = "";
            if ($key->type==1) {
                $url = "href='".base_url("divisions/".$key->division->slug)."'";
            }

            echo '
            <div class="slide background-overlay-gradient background-image" style="background-image:url(\''.base_url('assets/frontend/images/slides/'.$key->image).'\');">
                <div class="container">
                    <div class="slide-captions text-left text-light ">
                        <!-- Captions -->
                        </span>

                        <h1 style="font-size:40px;padding-top:300px;margin-bottom:0px;line-height:40px;">'.$key->name.'</h1>
                        <p style="font-size:20px">'.$key->description.'</p>

                        <!-- end: Captions -->
                    </div>
                </div>
            </div>
            ';
        }
    }
    ?>
</div>
<!--end: Inspiro Slider -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-md-9">
                <div class="row">
                    <div class="breadcrumb fancy m-l-15">
                        <ul>
                            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i></a> </li>
                            <li class="active"><a href="#"><?php echo $this->var_lang[$this->global_lang."title_page"] ?> : <?php echo $division->name ?></a> </li>
                        </ul>
                    </div>
                    <div class="col-md-6 p-t-10 ">
                        <h3 class=""><?php echo $this->var_lang[$this->global_lang."title_page"] ?> <?php echo $division->name ?></h3>
                        <p><?php echo $division->description ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 center">
                        <div class="row">
                            <div class="hr-title hr-long center">
                                <abbr><?php echo $this->var_lang[$this->global_lang."title_division_category"] ?></abbr>
                            </div>
                            <?php 
                            if (count($division->category)>0) {
                                foreach ($division->category as $key) {
                                    echo '
                                    <div class="col-md-3">
                                        <a href="'.base_url($key->url_prefix).'" type="button" class="btn btn-rounded btn-outline btn-reveal">'.$key->name.'</a>

                                    </div>
                                    ';
                                }
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
                <!--Product list-->
                <div class="shop">
                    <div class="hr-title hr-long center" style="margin-top:50px;">
                        <abbr><?php echo $this->var_lang[$this->global_lang."title_division_feature"] ?></abbr>
                    </div>
                    <div class="grid-layout grid-3-columns" data-item="grid-item">
                    <?php 
                    if (count($products)>0) {
                        foreach ($products as $key) {
                            echo '
                    <div class="grid-item">
                        <div class="product">
                            <div class="product-image">
                                <a style="width:265px;height:210px;" href="'.base_url($key->url_prefix).'"><img alt="'.$key->name.'" src="'.base_url($key->image_path.'medium_'.$key->image).'">
                                </a>
                                <div class="product-overlay">
                                    <a href="'.base_url($key->url_prefix).'" >Detail</a>
                                </div>
                            </div>

                            <div class="product-description">
                                <div class="product-category">'.$key->category->name.'</div>
                                <div class="product-title">
                                    <h3><a href="'.base_url($key->url_prefix).'">'.$key->name.'</a></h3>
                                </div>
                                <p>
                                '.$key->description.'
                                </p>
                            </div>
                        </div>
                    </div>
                            ';
                        }
                    }
                    ?>
                    </div>
                    
                    <hr>
                    <!-- Pagination -->
                    <div class="pagination">
                        <ul>
                            <?php 
                            echo $render_pagination;
                            ?>
                        </ul>
                    </div>
                    <!-- end: Pagination -->
                </div>
                <!--End: Product list-->
            </div>
            <!-- end: Content-->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <!--widget newsletter-->
                <div class="widget clearfix widget-archive">
                    <h4 class="widget-title"><?php echo $this->var_lang[$this->global_lang."title_division_category"] ?></h4>
                    <ul class="list list-lines">
                        <?php 
                        if (count($division->category)>0) {
                            foreach ($division->category as $key) {
                                echo '
                                <li><a href="'.base_url($key->url_prefix).'" >'.$key->name.'</a></li>
                                ';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="widget clearfix widget-tags">
                    <h4 class="widget-title">Tag</h4>
                    <div class="tags">
                        <?php 
                        if (count($tags)>0) {
                            foreach ($tags as $key) {
                                echo '
                                <a href="'.base_url($key->url_prefix).'">'.$key->name.'</a>
                                ';
                            }
                        }
                        ?>
                    </div>
                </div>


            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>