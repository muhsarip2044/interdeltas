<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">
                <!-- Page title -->
                <div class="page-title">
                    <h1><?php echo $title ?></h1>
                    <div class="breadcrumb float-left">
                        <ul>
                            <li><a href="<?php echo base_url(); ?>">Beranda</a>
                            </li>
                            <li><a href="<?php echo base_url('blogs'); ?>">Blog</a>
                            </li>
                            <li><a href="#"><?php echo $title ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- end: Page title -->

                <!-- Blog -->
                <div id="blog" class="post-thumbnails">
                    <?php
                    if (count($blog)>0) {
                        foreach ($blog as $key) {
                            echo '

                            <!-- Post item-->
                            <div class="post-item">
                                <div class="post-item-wrap">
                                    <div class="post-image">
                                        <a href="'.base_url($key->url_prefix).'">
                                            <img alt="" src="'.base_url($key->image_path."medium_".$key->image).'">
                                        </a>
                                        <span class="post-meta-category"><a href="#">'.$key->category->name.'</a></span>
                                    </div>
                                    <div class="post-item-description">
                                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>'.date('l, j F Y ',strtotime($key->created_at)).'</span>
                                        <h2>
                                            <a href="'.base_url($key->url_prefix).'">
                                            '.$key->name.'
                                            </a>
                                        </h2>
                                        <p>'.$key->summary.'</p>

                                        <a href="'.base_url($key->url_prefix).'" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                                    </div>
                                </div>
                            </div>
                            <!-- end: Post item-->
                            ';
                        }
                    }
                    ?> 
                </div>
            </div> 
            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <!--Tabs with Posts-->
                    <div class="widget ">
                        <h4 class="widget-title">Kategori Lain</h4>
                        <ul class="list list-lines">
                            <?php 
                            if (count($category)>0) {
                                foreach ($category as $key) {
                                    echo '
                                    <li><a href="'.base_url($key->url_prefix).'" >'.$key->name.'</a></li>
                                    ';
                                }
                            }
                            ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>