<!-- Shop products -->
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-md-9">
                <h3><?php echo $catalogue->name ?></h3> <br>

                <?php if(!empty($catalogue->image)): ?>
                <div style="width: 100%;text-align: center">
                    <div class="grid-item">
                        <div class="grid-item-wrap">
                            <div class="grid-image"> <img alt="<?php echo $catalogue->name ?>" src="<?php echo base_url($catalogue->image_path.$catalogue->image) ?>" /> </div>
                            <div class="grid-description">
                                <a title="<?php echo $catalogue->name ?>" data-lightbox="image" href="<?php echo base_url($catalogue->image_path.$catalogue->image) ?>" class="btn btn-light btn-rounded">Zoom</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <p>
                    <?php echo $catalogue->description; ?>
                </p>

                <?php if(!empty($catalogue->file)): ?>
                <div style="width: 100%;text-align: center">
                    <a href="<?php echo base_url("assets/frontend/files/catalogues/".$catalogue->file) ?>" target="_blank" class="btn btn-outline"><i class="fa fa-download"></i> Download Katalog</a>

                </div>
                <?php endif; ?>
            </div>
            <!-- end: Content-->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <!--widget newsletter-->
                <div class="widget clearfix widget-archive">
                    <h4 class="widget-title">Katalaog Lain</h4>
                    <ul class="list list-lines">
                        <?php 
                        if (count($division->catalogue)>0) {
                            foreach ($division->catalogue as $key) {
                                echo '
                                <li><a href="'.base_url($key->url_prefix).'" >'.$key->name.'</a></li>
                                ';
                            }
                        }
                        ?>
                    </ul>
                </div>


            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Shop products -->

