<style type="text/css">
	.col-centered{
	    float: none;
	    margin: 0 auto;
	}
</style>
<section class="background-grey">
    <div class="container">
        <div class="heading heading-center">
            <h2>Komisaris</h2>
            <span class="lead"><?php echo $this->component->company ?> memiliki <?php echo count($komisaris)   ?> Komisaris, diantaranya :</span>
        </div>
        <div class="row team-members team-members-shadow m-b-40 col-centered">
        	<div class="col-md-9 col-centered">
        		<div class="row">
        			<?php  
		        	if (count($komisaris)>0) {
		        		foreach ($komisaris as $key) {
		        			echo '
		        			<div class="col-md-4">
				                <div class="team-member">
				                    <div class="team-image">
				                        <img src="'.base_url($key->image_path.$key->image).'">
				                    </div>
				                    <div class="team-desc">
				                        <h3>'.$key->name.'</h3>
				                        <span>'.$key->position.'</span>
				                    </div>
				                </div>
				            </div>
		        			';
		        		}
		        	}
		        	?>	
        		</div>
        	</div>
        </div>
    </div>
</section>
<section class="">
    <div class="container">
        <div class="heading heading-center">
            <h2>Direksi</h2>
            <span class="lead"><?php echo $this->component->company ?> memiliki <?php echo count($direksi)   ?> Direksi, diantaranya :</span>
        </div>
        <div class="row team-members team-members-shadow m-b-40 col-centered">
        	<div class="col-md-9 col-centered">
        		<div class="row">
        			<?php  
		        	if (count($direksi)>0) {
		        		foreach ($direksi as $key) {
		        			echo '
		        			<div class="col-md-4">
				                <div class="team-member">
				                    <div class="team-image">
				                        <img src="'.base_url($key->image_path.$key->image).'">
				                    </div>
				                    <div class="team-desc">
				                        <h3>'.$key->name.'</h3>
				                        <span>'.$key->position.'</span>
				                    </div>
				                </div>
				            </div>
		        			';
		        		}
		        	}
		        	?>	
        		</div>
        	</div>
        </div>
    </div>
</section>