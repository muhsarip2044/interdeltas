<!-- CONTENT -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-uppercase">Divisi <?php echo $division->name ?></h3>
                <p><?php echo $division->description ?></p>


                <div class="row m-t-40">
                    <div class="col-md-6">
                        <address>
	                       <strong><?php echo $this->component->company; ?></strong><br> <br>
	                       <?php echo $this->component->address; ?><br> <br>
	                       <abbr title="Phone">Phone:</abbr> <?php echo $this->component->phone; ?><br> <br>
                           <abbr title="Phone">Ext: </abbr> <?php echo $division->phone_extension; ?>
	                   </address>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>




                <div class="social-icons m-t-30 social-icons-colored">
                    <ul>
                        <?php 
                        if (count($this->social_media)>0) {
                            foreach ($this->social_media as $key ) {
                                echo '
                        <li class="social-facebook"><a href="'.$key->url.'"><i class="fa '.$key->icon.'"></i></a>
                        </li>
                                ';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="col-md-6">
                <style type="text/css">
                    .form-group p {
                        color: red;
                        margin-bottom: 0px;
                    }
                </style>
                <?php 
                if ($submit_message_success == true) {
                    echo '
                    <div class="alert alert-info">
                        Pesan berhasil terkirim, terima kasih telah mengirimi pesan. Kami akan memblasanya secepat mungkin.
                    </div>
                    ';
                }
                ?>

                
                <form  action="<?php echo base_url("kontak-kami/".$division->slug) ?>" role="form" method="post">
                    <input type="hidden" value="<?php echo $division->id ?>" name="division_id">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="name">Name</label>
                            <input type="text" aria-required="true" name="name" class="form-control required name"  placeholder="Enter your Name" required>
                            <?php echo form_error('name'); ?>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" aria-required="true" name="email" class="form-control required email" placeholder="Enter your Email" required>
                            <?php echo form_error('email'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="subject">Your Subject</label>
                            <input type="text" name="subject" class="form-control required" placeholder="Subject..." required>
                            <?php echo form_error('subject'); ?>
                        </div>
                    </div>                                <div class="form-group">
                            <label for="message">Message</label>
                            <textarea type="text" name="message" rows="5" class="form-control required" placeholder="Enter your Message" required></textarea>
                            <?php echo form_error('message'); ?>
                        </div>

                         <div class="form-group">
                              <script src='../../www.google.com/recaptcha/api.js'></script>
                            <div class="g-recaptcha" data-sitekey="6LddCxAUAAAAAKOg0-U6IprqOZ7vTfiMNSyQT2-M"></div>
                        </div>
                    <button class="btn btn-default" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Kirim pesan ke Divisi <?php echo $division->name ?></button>
                    
                </form>
                
            </div>

        </div>
    </div>
</section>
<!-- end: CONTENT -->