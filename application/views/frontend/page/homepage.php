<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="360">
    <?php 
    if (count($slide)>0) {
        foreach ($slide as $key ) {
            $url = "";
            if ($key->type==1 and count($key->division)>0) {
                $url = "href='".base_url("divisions/".$key->division->slug)."'";
            }
            echo '
            <div class="slide background-overlay-gradient background-image" style="background-image:url(\''.base_url('assets/frontend/images/slides/'.$key->image).'\');">
                <div class="container">
                    <div class="slide-captions text-left text-light ">
                        <!-- Captions -->
                        </span>

                        <h1 style="font-size:40px;padding-top:250px;margin-bottom:0px;line-height:40px;">'.$key->name.'</h1>
                        <p style="font-size:20px">'.$key->description.'</p>

                        <!-- end: Captions -->
                    </div>
                </div>
            </div>
            ';
        }
    }
    ?>

</div>
<!--end: Inspiro Slider -->
<section>
    <div class="container">
        <div class="heading heading-center">
            <h2><?php echo $homepage->segment_1_title ?></h2>
            <p><?php echo $homepage->segment_1_description ?></p>
        </div>

        <div class="row">
            <?php 
            if (count($company_feature)>0) {
                foreach ($company_feature as $key) {
                    echo '
                    <div class="col-md-4">
                        <div class="icon-box effect medium border center">
                            <div class="icon">
                                <a href="#"><i class="fa fa-'.$key->icon.'"></i></a>
                            </div>
                            <h3>'.$key->name.'</h3>
                            <p>'.$key->description.'</p>
                        </div>
                    </div>
                    ';
                }
            }
            ?>
        </div>
    </div>

</section>

<!-- Call to action -->
<section class="background-grey text-center" style="">
    <div class="container">
        <div class="col-md-12">
            <h3 style="font-weight: 100;"><?php echo $homepage->segment_2_description ?></h3>
        </div>
    </div>
</section>
<!-- end: Call to action -->
<section style="padding-bottom: 0px;">
    <div class="container">
        <h4 class="m-b-30 text-center"><?php echo $this->var_lang[$this->global_lang."title_product"] ?></h4>
        <!--Product list-->
        <div class="shop">
            <div class="grid-layout grid-3-columns" data-item="grid-item">
            <?php 
            for ($i=0; $i < 3  ; $i++) { 
                if (count($product_division[$i]))
                {
                    foreach ($product_division[$i] as $key) {
                        echo '
                        <div class="grid-item">
                            <div class="product">
                                <div class="product-image text-center" >
                                    <a style="width:265px;height:210px;margin:auto;" href="'.base_url($key->url_prefix).'"><img  alt="'.$key->name.'" src="'.base_url($key->image_path.'medium_'.$key->image).'">
                                    </a>
                                    <div class="product-overlay">
                                        <a href="'.base_url($key->url_prefix).'" >Detail</a>
                                    </div>
                                </div>

                                <div class="product-description">
                                    <div class="product-category">'.$key->category->name.'</div>
                                    <div class="product-title">
                                        <h3><a href="'.base_url($key->url_prefix).'">'.$key->name.'</a></h3>
                                    </div>
                                    <p>
                                    '.$key->description.'
                                    </p>
                                </div>
                            </div>
                        </div>
                        ';
                    }
                } 
            }

            ?>
            </div>
            <!-- end: Pagination -->
        </div>
        <!--End: Product list-->
    </div>
</section>


<section >
    <div class="container">
        <h4 class="m-b-30 text-center"><?php echo $this->var_lang[$this->global_lang."title_news"] ?></h4>
        <!-- Blog -->
        <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">

            <?php
            if (count($blog)>0) {
                foreach ($blog as $key) {
                    echo '
                    <!-- Post item-->
                    <div class="post-item border">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="'.base_url($key->url_prefix).'">
                                    <img  alt="" src="'.base_url($key->image_path."medium_".$key->image).'">
                                </a>
                                <span class="post-meta-category"><a href="#">'.$key->category->name.'</a></span>
                            </div>
                            <div class="post-item-description">
                                <span class="post-meta-date"><i class="fa fa-calendar-o"></i>'.date('l, j F Y ',strtotime($key->created_at)).'</span>
                                <h2>
                                    <a href="'.base_url($key->url_prefix).'">
                                        '.$key->name.'
                                    </a>
                                </h2>
                                <p>'.$key->summary.'</p>

                                <a href="'.base_url($key->url_prefix).'" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                            </div>
                        </div>
                    </div>
                    <!-- end: Post item-->
                    ';
                }
            }
            ?> 

        </div>
    </div>
</section>