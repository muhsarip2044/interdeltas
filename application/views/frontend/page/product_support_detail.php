<!-- Page Content -->
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div class="row">
            <!-- content -->
            <div class="content col-md-9">
                <!-- Blog -->
                <div id="blog" class="single-post">
                    <!-- Post single item-->
                    <div class="post-item">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="#">
                                    <img alt="" src="<?php echo base_url($detail->image_path.$detail->image) ?>">
                                </a>
                            </div>
                            <div class="post-item-description">
                                <h2><?php echo $detail->name ?></h2>
                                <div class="post-meta">

                                    <span class="post-meta-date"><i class="fa fa-calendar-o"></i><?php echo date('l, j F Y ',strtotime($detail->created_at)) ?></span>
                                    <div class="post-meta-share">
                                        <div class="align-center">
                                            <div class="fb-share-button" data-href="<?php echo base_url(uri_string()); ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Bagikan</a></div>
                                        </div>
                                    </div>
                                </div>
                                <p><?php echo $detail->description ?></p>

                            </div>
                        </div>
                    </div>
                    <!-- end: Post single item-->
                </div>

            </div>
            <!-- end: content -->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <div class="widget ">
                        <h4 class="widget-title">Other Support Info</h4>
                        <ul class="list list-lines">
                            <?php 
                            if (count($other_info)>0) {
                                foreach ($other_info as $key) {
                                    echo '
                                    <li><a href="'.base_url($key->url_prefix).'" >'.$key->name.'</a></li>
                                    ';
                                }
                            }
                            ?>
                        </ul>

                    </div>


                </div>
            </div>
            <!-- end: sidebar-->
        </div>
    </div>
</section>
<!-- end: Page Content -->