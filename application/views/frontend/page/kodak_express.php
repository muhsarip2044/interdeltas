<!-- Page title -->
<section id="page-title" data-parallax-image="<?php echo base_url("assets/frontend/images/".$detail->image) ?>">
	<div class="container">
		<div class="page-title">
			<h1><?php echo $detail->name ?></h1>
			<span></span>
		</div>
		<div class="breadcrumb">
					<ul>
				<li><a href="<?php echo base_url() ?>">Beranda</a>
				</li>
				<li><a href="#"><?php echo $detail->name ?></a>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- end: Page title -->
<!-- CONTENT -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4"> 
                <h3 class="text-uppercase"><?php echo $detail->name ?></h3>
                <p>
                    <?php echo $detail->description ?>
                </p>
            </div>
            <div class="col-md-8">
                <?php 
                if (count($region)>0) {
                    foreach ($region as $key ) {
                        echo '
                        <h3 style="color:#ef9e10;">'.$key->name.'  <small>'.count($key->outlet).' Outlet</small></h3>
                        <div class="row">
                        ';
                        if (count($key->outlet)>0) {
                            foreach ($key->outlet as $keys) {
                                echo '
                                <div class="col-md-6">
                                    <h4><strong>'.$keys->outlet_name.'</strong></h4>
                                    <address>
                                      '.$keys->address.' <br>
                                      </address>
                                </div>
                                ';
                            }
                        }
                        echo '
                        </div>
                        ';
                    }
                }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- end: CONTENT -->

