<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9" style="min-height: 500px;">
                <!-- Page title -->
                <div class="page-title" >
                    <h4>
                        Hasil pencarian <strong><i><?php echo $keyword; ?></i></strong> <br>
                        <small>Menemukan <?php echo count($products) ?> produk.</small>
                    </h4>
                </div>
                <!-- end: Page title -->
                <!--Product list-->
                <div class="shop">
                    <div class="grid-layout grid-3-columns" data-item="grid-item">
                    <?php 
                    if (count($products)>0) {
                        foreach ($products as $key) {
                            echo '
                    <div class="grid-item">
                        <div class="product">
                            <div class="product-image">
                                <a style="width:265px;height:210px;" href="'.base_url($key->url_prefix).'"><img alt="Shop product image!" src="'.base_url($key->image_path.'medium_'.$key->image).'">
                                </a>
                                <div class="product-overlay">
                                    <a href="'.base_url($key->url_prefix).'" >Detail</a>
                                </div>
                            </div>

                            <div class="product-description">
                                <div class="product-category">'.$key->category->name.'</div>
                                <div class="product-title">
                                    <h3><a href="'.base_url($key->url_prefix).'">'.$key->name.'</a></h3>
                                </div>
                                <p>
                                '.$key->description.'
                                </p>
                            </div>
                        </div>
                    </div>
                            ';
                        }
                    }else{
                        echo '
                        <br>
                        <h5 style="color:gray">Hasil tidak ditemukan.</h5>
                        ';
                    }
                    ?>
                    </div>
                    
                    <hr>
                </div>

                
            </div> 
            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <!--Tabs with Posts-->
                    <div class="widget ">
                        <h4 class="widget-title">Kategori Produk</h4>
                        <ul class="list list-lines">
                            <?php 
                            if (count($category)>0) {
                                foreach ($category as $key) {
                                    echo '
                                    <li><strong style="color:#f1c40f">'.$key->name.'</strong></li>
                                    ';

                                    if (count($key->category)>0) {
                                        foreach ($key->category as $key2) {
                                            echo '
                                            <li><a href="'.base_url($key2->url_prefix).'">'.$key2->name.'</a></li>
                                            ';
                                        }
                                    }
                                }
                            }
                            ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>