<?php 
$lang_name = $this->global_lang."name";
?>
<!-- Page title -->
<section id="page-title" style="background:#f39c12;height:0px;" data-parallax-image="<?php echo base_url("assets/frontend/images/city.jpg") ?>">
	<div class="container">
		<div class="page-title">
			<h1><?php echo $detail->$lang_name ?></h1>
			<span></span>
		</div>
		<div class="breadcrumb">
					<ul>
				<li><a href="<?php echo base_url() ?>">Beranda</a>
				</li>
				<li><a href="#"><?php echo $detail->$lang_name ?></a>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- end: Page title -->
<!-- CONTENT -->
<section>
    <div class="container">
        <div class="row m-t-40">
            <div class="col-md-2">
            	<p>Share this page</p>
                <div class="align-center">
                	<div class="fb-share-button" data-href="<?php echo base_url(uri_string()); ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Bagikan</a></div>
				</div>
            </div>
            <div class="col-md-10">
            	<?php if(!empty($detail->image)){ ?>
            	<div  >
            		<img class="img img-responsive" src="<?php echo base_url("assets/frontend/images/".$detail->image) ?>">
            	</div>
            	<?php } ?>
            	<div style="padding-top:40px;">
            		<?php 
                    
                    $lang_desc = $this->global_lang."description";
                    echo $detail->$lang_desc;
                    
                    ?>
            	</div>
            </div>
        </div>
    </div>
</section>

