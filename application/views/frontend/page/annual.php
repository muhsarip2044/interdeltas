<!-- Page title -->
<section id="page-title" data-parallax-image="<?php echo base_url("assets/frontend/images/bg-contact.jpg") ?>">
	<div class="container">
		<div class="page-title">
			<h1>Annual Report</h1>
			<span></span>
		</div>
		<div class="breadcrumb">
					<ul>
				<li><a href="<?php echo base_url() ?>">Beranda</a>
				</li>
				<li><a href="#">Annual Report</a>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- end: Page title -->
<!-- CONTENT -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                    	<p>Share this page</p>
                        <div class="align-center">
						    <a class="btn btn-xs btn-slide btn-facebook" href="#">
						        <i class="fa fa-facebook"></i>
						        <span>Facebook</span>
						    </a>
						    <a class="btn btn-xs btn-slide btn-twitter" href="#" data-width="100" style="width: 28px;">
						        <i class="fa fa-twitter"></i>
						        <span>Twitter</span>
						    </a>
						    <a class="btn btn-xs btn-slide btn-instagram" href="#" data-width="118" style="width: 28px;">
						        <i class="fa fa-instagram"></i>
						        <span>Instagram</span>
						    </a>
						    <a class="btn btn-xs btn-slide btn-googleplus" href="mailto:#" data-width="80" style="width: 28px;">
						        <i class="fa fa-envelope"></i>
						        <span>Mail</span>
						    </a>
						</div>
                    </div>
                    <div class="col-md-9">
						<div class="accordion">
							<?php 
							if (count($report)>0) {
								foreach ($report as $key) {
									echo '
									<div class="ac-item">
										<h5 class="ac-title">'.$key->name.'</h5>
										<div class="ac-content">
											<a href="'.base_url($key->file_path.$key->file).'" target="_blank" alt="Download Report">
												<img class="img img-responsive" src="'.base_url("assets/frontend/images/reports/".$key->image).'">
											</a>
											
										</div>
									</div>
									';
								}
							}
							?>
						</div>

                    </div>
        </div>
    </div>
</section>

