<!-- Shop products -->
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div class="row">
            <div class="breadcrumb fancy m-l-15">
                <ul>
                    <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i></a> </li>
                    <li><a href="<?php echo base_url("divisions/".$product->division->slug); ?>">Division : <?php echo $product->division->name ?></a> </li>
                    <li><a href="<?php echo base_url("categories/".$product->division->slug."/".$product->category->slug); ?>">Category : <?php echo $product->category->name ?></a> </li>
                    <?php 
                    if ($product->brand != null) {
                        echo '<li class="active"><a href="#">'.$product->brand->name.'</a> </li>';
                    }
                    ?>
                    
                </ul>
            </div>
            <!-- Content-->
            <div class="content col-md-9">
                <div class="product">
                    <div class="row m-b-40">
                        <div class="col-md-5">
                            <div class="product-image">
                                <!-- Carousel slider -->
                                <div class="carousel dots-inside dots-dark arrows-visible arrows-only arrows-dark" data-items="1" data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-autoplay-timeout="2500" data-lightbox="gallery">
                                    <a href="<?php echo base_url($product->image_path.'large_'.$product->image) ?>" data-lightbox="image" title="<?php echo $product->name ?>"><img  alt="<?php echo $product->name ?>" src="<?php echo base_url($product->image_path.'large_'.$product->image) ?>">
                                    </a>
                                    <a href="<?php echo base_url($product->image_path.'large_'.$product->image) ?>" data-lightbox="image" title="<?php echo $product->name ?>"><img  alt="<?php echo $product->name ?>" src="<?php echo base_url($product->image_path.'large_'.$product->image) ?>">
                                    </a>
                                </div>
                                <!-- Carousel slider -->
                            </div>
                        </div>


                        <div class="col-md-7">
                            <div class="product-description">
                                <div class="product-category"><?php echo $product->category->name ?></div>
                                <div class="product-title">
                                    <h2><a href="#"><?php echo $product->name ?></a></h2>
                                </div>
                                <div class="product-price"><ins></ins>
                                </div>
                                <div class="product-review" style="color:#333;">
                                    <?php 
                                    if ($product->brand != null) {
                                        echo '
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <img class="img img-responsive" src="'.base_url("assets/frontend/images/".$product->brand->image).'" style="height:50px;width:50px;" >
                                            </div>
                                            <div class="col-lg-8" style="padding-top:10px;">
                                                '.$product->brand->name.'

                                            </div>
                                        </div>
                                        ';
                                    }
                                    ?>
                                </div>
                                <div class="product-reviews" style="display: none;"><a href="#">3 Kali Dilihat</a>
                                </div>

                                <div class="seperator m-b-10"></div>
                          
                                <div style="margin:20px 0px 20px 0px;">
                                    <span style="color:#d35400">Description:</span> <br>
                                    <?php 
                                    if ($product->description != '') {
                                        echo $product->description;     
                                    }else{
                                        echo "-";
                                    }
                                    
                                    ?>
                                </div>
                                <div style="margin:20px 0px 20px 0px;">
                                    <span style="color:#d35400">Specification:</span><br>
                                    <div></div>
                                    <?php  
                                    if ($specification->num_rows()>0) {

                                        foreach ($specification->result() as $key) {
                                            echo "<p>".$key->option." : ".$key->value."</p>";
                                        }
                                    }else{
                                        echo "-";
                                    }
                                    ?>
                                </div>
                                <div class="product-meta">
                                    <span style="color:#d35400">Tags:</span> <br>
                                    <div class="tags" style="padding-top:10px;">
                                    <?php 
                                    if (count($product->tags)>0) {
                                        foreach ($product->tags as $key) {
                                            echo '
                                            <a href="#" rel="'.$key->tag->name.'">'.$key->tag->name.'</a>, 
                                            ';
                                        }
                                    }else{
                                        echo "-";
                                    }
                                    ?>
                                    </div>
                                </div>
                                <div class="seperator m-t-20 m-b-10"></div>

                            </div>


                        </div>
                    </div>

                    <div id="tabs-1" class="tabs simple">
                        <ul class="tabs-navigation">
                            <li class="active"><a href="#tab1"><i class="fa fa-video-camera"></i>Video Product</a> </li>
                            <li><a href="#tab2"><i class="fa fa-info"></i>Additional Info</a> </li>
                            <li><a href="#tab3"><i class="fa fa-star"></i>Reviews</a> </li>
                        </ul>
                        <div class="tabs-content">
                            <div class="tab-pane active" id="tab1">
                                <style type="text/css">
                                    video {
                                      width: 100%    !important;
                                      height: auto   !important;
                                    }
                                </style>
                                <?php  
                                if (!empty($product->video_type)) {
                                    if ($product->video_type == "Youtube") {
                                        echo ''.$product->youtube_source.'';
                                    }else if($product->video_type == "Local"){
                                        echo '
                                        <video width="320" height="240" controls>
                                          <source src="'.base_url($product->video_path.$product->local_source).'" type="video/mp4">
                                        Your browser does not support the video tag.
                                        </video>
                                        ';
                                    }else{
                                        echo '<p>Video produk tidak tersedia.</p>';
                                    }
                                }else{
                                    echo '<p>Video produk tidak tersedia.</p>'; 
                                }
                                ?>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <?php
                                if (!empty($product->additional_info)) {
                                    echo $product->additional_info;
                                }else{
                                    echo '<p>Additional info tidak tersedia.</p>';
                                }
                                ?>
                            </div>
                            <div class="tab-pane" id="tab3">
                                <?php
                                if (!empty($product->reviews)) {
                                    echo $product->reviews;
                                }else{
                                    echo '<p>Reviews tidak tersedia.</p>';
                                }
                                ?>
                            </div>


                        </div>
                    </div>
                    <div class="">
                        <div class="align-center">
                            <div class="fb-share-button" data-href="<?php echo base_url(uri_string()); ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Bagikan</a></div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end: Content-->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <!--widget newsletter-->
                <div class="widget clearfix widget-archive">
                    <h4 class="widget-title">Product categories</h4>
                    <ul class="list list-lines">
                        <?php 
                        if (count($division->category)>0) {
                            foreach ($division->category as $key) {
                                echo '
                                <li><a href="'.base_url($key->url_prefix).'" >'.$key->name.'</a></li>
                                ';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="widget clearfix widget-tags">
                    <h4 class="widget-title">Tags</h4>
                    <div class="tags">
                        <?php 
                        if (count($tags)>0) {
                            foreach ($tags as $key) {
                                echo '
                                <a href="'.base_url($key->url_prefix).'">'.$key->name.'</a>
                                ';
                            }
                        }
                        ?>
                    </div>
                </div>


            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Shop products -->

