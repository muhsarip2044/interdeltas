<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="<?php echo $this->component->seo_author ?>" />
    <meta name="description" content="<?php echo $this->component->seo_description ?>">
    <meta name="keywords" content="<?php echo $this->component->seo_keyword ?>">
    <!-- Document title -->
    <title>
        <?php echo $title ?>
    </title>
    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url("assets/frontend/images/".$this->component->favicon) ?>"/>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/frontend/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/frontend/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/frontend/css/responsive.css" rel="stylesheet">
</head>

<body>
    <!-- facebook script -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.9";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- end of facebook script -->


    <!-- Wrapper -->
    <div id="wrapper">


        <!-- Header -->
        <header id="header" class="header-transparent header-fullwidth dark" style="background: #ffb901">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="<?php echo base_url() ?>" class="logo" data-dark-logo="<?php echo base_url('assets/frontend/images/'.$this->component->logo_white) ?>">
                            <img style="max-width: 200px;height: auto;margin-top: 15px;" src="<?php echo base_url('assets/frontend/images/'.$this->component->logo) ?>" alt="<?php echo $this->component->company ?>">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Top Search Form-->
                    <div id="top-search">
                        <form action="<?php echo base_url("search") ?>" method="post">
                            <input type="text" name="keyword" class="form-control" value="" placeholder="Ketik untuk mencari produk">
                        </form>
                    </div>
                    <!--end: Top Search Form-->

                    <!--Header Extras-->
                    <div class="header-extras">
                        <ul>
                            <li>
                                <!--top search-->
                                <a id="top-search-trigger" href="#" class="toggle-item">
                                    <i class="fa fa-search"></i>
                                    <i class="fa fa-close"></i>
                                </a>
                                <!--end: top search-->
                            </li>
                            <li>
                                <div class="topbar-dropdown">
                                    <a class="title"><i class="fa fa-globe"></i></a>
                                    <div class="dropdown-list">
                                        <a href="<?php echo base_url("home/change_language/0") ?>" class="list-entry">Bahasa</a>
                                        <a href="<?php echo base_url("home/change_language/en_") ?>" class="list-entry" >English</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--end: Header Extras-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <?php 
                                    $lang_name = $this->global_lang."name";
                                    if (count($menu_homepage)>0) {
                                        //
                                        foreach ($menu_homepage as $key) {
                                            $url = "";
                                            if ($key->type == 1) {
                                                $url = "divisions/".$key->division->slug;
                                                $name = $key->division->name;
                                            }else{
                                                $url = "page/".$key->slug;

                                                $name = $key->$lang_name;
                                            }
                                            if (count($key->children)>0) {
                                                echo '
                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                            '.$name.'
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                ';
                                                foreach ($key->children as $key2 ) {
                                                    if ($key2->slug == "investor") {
                                                        echo '
                                                        <li class="dropdown-submenu"><a href="#">'.$key2->$lang_name.'</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="'.base_url("page/annual-report").'">Annual Report</a></li>
                                                                <li><a href="'.base_url("page/financial-report").'">Financial Report</a></li>
                                                            </ul> 
                                                        </li>


                                                        ';
                                                    }else{
                                                        echo '
                                                            <li><a href="'.base_url("page/".$key2->slug).'">'.$key2->$lang_name.'</a></li>
                                                        ';
                                                    }
                                                    
                                                }
                                                echo '
                                                        </ul>
                                                    </li>
                                                ';
                                            }else{
                                                echo '
                                                    <li><a href="'.base_url($url).'">'.$name.'</a></li>
                                                ';
                                            }
                                            
                                        }
                                    }
                                    ?>
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->


        <?php echo $content ?>

        <?php $this->load->view('frontend/component/footer'); ?>

    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
    <!--Plugins-->
    <script src="<?php echo base_url() ?>assets/frontend/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>assets/frontend/js/plugins.js"></script>

    <!--Template functions-->
    <script src="<?php echo base_url() ?>assets/frontend/js/functions.js"></script>

</body>
</html>