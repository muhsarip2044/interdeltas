<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="<?php echo $this->component->seo_author ?>" />
    <meta name="description" content="<?php echo $this->component->seo_description ?>">
    <meta name="keywords" content="<?php echo $this->component->seo_keyword ?>">
    <!-- Document title -->
    <title>
        <?php echo $title ?>
    </title>
    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url("assets/frontend/images/".$this->component->favicon) ?>"/>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/frontend/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/frontend/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/frontend/css/responsive.css" rel="stylesheet">
   
</head>

<body>
    <script src="<?php echo base_url() ?>assets/frontend/js/jquery.js"></script>
    <!-- facebook script -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.9";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- end of facebook script -->


    <!-- Wrapper -->
    <div id="wrapper"> 


        <?php echo $header_navbar; ?>


        <?php echo $content ?>

        <?php $this->load->view('frontend/component/footer'); ?>

    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
    <!--Plugins-->
    
    <script src="<?php echo base_url() ?>assets/frontend/js/plugins.js"></script>

    <!--Template functions-->
    <script src="<?php echo base_url() ?>assets/frontend/js/functions.js"></script>

</body>
</html>