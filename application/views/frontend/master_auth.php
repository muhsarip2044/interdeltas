<!-- <!DOCTYPE html> -->
<html lang="en">

<head>
    <title>Inter Delta</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- facebook graph -->
    <meta name="author" content="<?php echo $this->component->seo_author ?>" />
    <meta name="description" content="<?php echo $this->component->seo_description ?>">
    <meta name="keywords" content="<?php echo $this->component->seo_keyword ?>">
    <link href="<?php echo base_url() ?>assets/frontend/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/frontend/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/frontend/css/responsive.css" rel="stylesheet">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/frontend/images/'.$this->component->favicon) ?>"/>

    <!-- font-awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/frontend/vendors/font-awesome/css/font-awesome.min.css">
    <!-- font roboto -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- font roboto -->
    <link href="https://fonts.googleapis.com/css?family=Advent+Pro" rel="stylesheet">
    <!-- main style  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/frontend/css/style.css">
</head>

<body>

<style type="text/css">
    .auth-area { 
      width: 100%;height: 100%;
    }
</style>
<div class="container-fluid auth-area" >
  <img src="<?php echo base_url('assets/frontend/images/bg-auth.png') ?>" style="width: 100%;height: auto;position: fixed;bottom: 0px;left: 0px;right: 0px;">
    <?php echo $content; ?>
</div>
<script src="<?php echo base_url() ?>assets/frontend/js/jquery.js"></script>
</body>

</html>