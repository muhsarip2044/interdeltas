<!-- Footer -->
<footer id="footer" class="footer-light">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <!-- Footer widget area 1 -->
                    <div class="widget clearfix widget-contact-us" style="background-image: url('images/world-map-dark.png'); background-position: 50% 20px; background-repeat: no-repeat">
                        <h4><?php echo $this->var_lang[$this->global_lang."footer_about"] ?></h4>
                        <ul class="list-icon">
                            <li><i class="fa fa-map-marker"></i> <?php echo $this->component->address ?></li>
                            <li><i class="fa fa-phone"></i> <?php echo $this->component->phone ?> </li>
                            <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $this->component->email ?>"><?php echo $this->component->email ?></a>
                            </li>
                            <li>
                                <br><i class="fa fa-clock-o"></i> <?php echo $this->component->office_hour ?>
                            </li>
                        </ul>
                        <!-- Social icons -->
                        <div class="social-icons social-icons-border float-left m-t-20">
                            <ul>
                                <?php 
                                if (count($this->social_media)>0) {
                                    foreach ($this->social_media as $key ) {
                                        echo '
                                <li class="social-rss"><a href="'.$key->url.'"><i class="fa '.$key->icon.'"></i></a>
                                </li>
                                        ';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <!-- end: Social icons -->
                    </div>
                    <!-- end: Footer widget area 1 -->
                </div>
                <div class="col-md-4 hidden-xs">
                    <!-- Footer widget area 2 -->
                    <div class="widget">
                        <h4><?php echo $this->var_lang[$this->global_lang."footer_link"] ?></h4>
                        <ul class="list-icon list-icon-arrow">
                            <?php 
                            if (count($this->quick_link)>0) {
                                foreach ($this->quick_link as $key) {
                                    if (count($key->division)>0) {
                                        echo '<li><a href="'.base_url("divisions/".$key->division->slug).'">'.$key->division->name.'</a>';
                                    }else{
                                        echo '<li><a href="'.base_url($key->slug).'">'.$key->name.'</a>';
                                    }
                                    
                                }
                            }

                            ?>
                        </ul>
                    </div>
                    <!-- end: Footer widget area 2 -->
                </div>
                <div class="col-md-4 hidden-xs">
                    <!-- Footer widget area 3 -->
                    <div class="widget">
                        <h4><?php echo $this->var_lang[$this->global_lang."footer_article"] ?></h4>
                        <div class="post-thumbnail-list">
                            <?php 
                            if (count($this->last_article)>0) {
                                foreach ($this->last_article as $key) {
                                    echo '
                                    <div class="post-thumbnail-entry">

                                        <div class="post-thumbnail-content">
                                            <a href="'.base_url($key->url_prefix).'">'.$key->name.'</a>
                                            <span class="post-date"><i class="fa fa-clock-o"></i> '.date('l, j F Y ',strtotime($key->created_at)).'</span>
                                            <span class="post-category"><i class="fa fa-tag"></i>'.$key->category->name.'</span>
                                        </div>
                                    </div>
                                    ';
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <!-- end: Footer widget area 3 -->
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-content">
        <div class="container">
            <div class="copyright-text text-center"><?php echo $this->component->footer ?>
            </div>
        </div>
    </div>
</footer>
<!-- end: Footer -->