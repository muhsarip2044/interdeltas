<!-- <!DOCTYPE html> -->
<html lang="en">

<head>
    <title>Inter Delta</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- facebook graph -->
    <meta property="og:url" content="travel.com" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Travel" />
    <meta property="og:description" content="Pesen tiket murah, aman dan mudah pembayaran" />
    <meta property="og:image" content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" />

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend/css/bootstrap.min.css">
    <script src="<?php echo base_url() ?>assets/frontend/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/frontend/js/bootstrap.min.js"></script>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/frontend/images/'.$this->component->favicon) ?>"/>

    <!-- font-awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/frontend/vendors/font-awesome/css/font-awesome.min.css">
    <!-- font roboto -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- font roboto -->
    <link href="https://fonts.googleapis.com/css?family=Advent+Pro" rel="stylesheet">
    <!-- main style  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/frontend/css/style.css">
</head>

<body>

<style type="text/css">
    .auth-area { 
      width: 100%;height: 100%;
    }
</style>
<div class="container-fluid auth-area" >
  <img src="<?php echo base_url('assets/frontend/images/bg-auth.png') ?>" style="width: 100%;height: auto;position: fixed;bottom: 0px;left: 0px;right: 0px;">
    <?php echo $content; ?>
</div>
</body>

</html>