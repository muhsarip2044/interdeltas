<!-- <!DOCTYPE html> -->
<html lang="en">

<head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- facebook graph -->
    <meta property="og:url" content="inter-delta.com" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend/css/bootstrap.min.css">
    <script src="<?php echo base_url() ?>assets/frontend/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/frontend/js/bootstrap.min.js"></script>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/frontend/images/'.$this->component->favicon) ?>"/>

    <!-- font-awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/frontend/vendors/font-awesome/css/font-awesome.min.css">
    <!-- font roboto -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- font roboto -->
    <link href="https://fonts.googleapis.com/css?family=Advent+Pro" rel="stylesheet">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/frontend/vendors/swiper/css/swiper.min.css">
    <!-- main style  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/frontend/css/style.css">
</head>

<body>


    <!-- HEAD SECTION -->
    <section id="header">
        <nav class="navbar  navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
              </button>
              <a class="navbar-brand" href="<?php echo base_url() ?>"><?php echo $this->component->title ?></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo base_url() ?>">Beranda</a>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Katalog <span class="caret"></span></a>
                      <ul class="dropdown-menu">

                        <li><a href="#">Page 1-1</a></li>
                        <li><a href="#">Page 1-2</a></li>
                        <li><a href="#">Page 1-3</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Produk <span class="caret"></span></a>
                      <ul class="dropdown-menu">

                        
                        <?php 
                        if (count($division->category)>0) {
                            foreach ($division->category as $key) {
                                echo '
                                <li><a href="'.base_url($key->url_prefix.$division->slug."/".$key->slug).'">'.$key->name.'</a></li>
                                ';
                            }
                        }
                        ?>
                      </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url("kontak-kami/".$division->slug) ?>">Kontak Kami</a>
                    </li>
                </ul>
            </div>
          </div>
        </nav>
    </section>
    <!-- END OF HEAD SECTION -->

    <!-- SECTION CONTENT -->
    <section id="content" style="min-height: 100%">
        <?php echo $content; ?>
    </section>
    
    <!-- END OF SECTION CONTENT -->



    <!-- SECTION FOOTER -->
    <style type="text/css">
        #footer {
            text-align: center;
            width: 100%;
            background: #e7e7e7;
        }
        .footer-field ul {
            list-style: none;
        }
        .footer-copyright {
            width: 100%;
            text-align: center;
        }
    </style>
    <section id="footer">
        <!-- footer -->
        <div class="footer" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 footer-field">
                        <h3>
                        
                            Alamat
                        </h3>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker"></i> <?php echo $this->component->address ?>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i> <?php echo $this->component->phone ?>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i> <?php echo $this->component->email ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 footer-field">
                        <h3>
                        Kontak Kami
                    </h3>
                        <ul>
                            <?php 
                            if (count($this->social_media)>0) {
                                foreach ($this->social_media as $key ) {
                                    echo '
                                    <li>
                                        <a href="'.$key->url.'">
                                            <i class="fa '.$key->icon.'"></i> '.$key->name.'
                                        </a>
                                    </li>
                                    ';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="col-lg-4 footer-field">
                        
                    </div>
                </div>
                <div class="footer-copyright">
                <?php echo $this->component->footer ?>
                </div>
            </div>

        </div>
    </section>

    <!-- END OF SECTION FOOTER -->

    <!-- Swiper JS -->
    <script src="<?php echo base_url() ?>assets/frontend/vendors/swiper/js/swiper.min.js"></script>

    <!-- Main Function -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/frontend/js/function.js"></script>
    <script type="text/javascript">
        /*
        * Init slider plugin
        */
            var swiper = new Swiper('.swiper-container', {
                loop:true,
                autoplay: 4000,
                speed:1000,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
            });

        
    </script>

</body>

</html>