<!DOCTYPE html>
<html>
<head>
	<title>Upps, Halaman anda yang anda minta tidak bisa kami tampilkan.</title>
</head>
<body marginwidth="0" marginheight="0">
<style type="text/css">
	  html{
  }
  body{
      margin: 0;
      padding: 0;
      background: #e7ecf0;
      font-family: Arial, Helvetica, sans-serif;
  }
  *{
      margin: 0;
      padding: 0;
  }
  p{
      font-size: 12px;
      color: #373737;
      font-family: Arial, Helvetica, sans-serif;
      line-height: 18px;
  }
  p a{
      color: #218bdc;
      font-size: 12px;
      text-decoration: none;
  }
  a{
      outline: none;
  }
  .f-left{
      float:left;
  }
  .f-right{
      float:right;
  }
  .clear{
      clear: both;
      overflow: hidden;
  }
  #block_error{
      width: 845px;
      height: 384px;
      border: 1px solid #cccccc;
      margin: 72px auto 0;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
      border-radius: 4px;
      background: #fff url(http://www.ebpaidrev.com/systemerror/block.gif) no-repeat 0 51px;
  }
  #block_error div{
      padding: 100px 40px 0 186px;
  }
  #block_error div h2{
      color: #218bdc;
      font-size: 24px;
      display: block;
      padding: 0 0 14px 0;
      border-bottom: 1px solid #cccccc;
      margin-bottom: 12px;
      font-weight: normal;
  }
</style>
    <div id="block_error">
        <div>
         <h2>Error 404. Upps, Halaman anda yang anda minta tidak bisa kami tampilkan.</h2>
        <p style="min-height: 30px;">
        </p>
        <h3 style="color: red">
        	<?php echo $description; ?>
        </h3>
        <h3 style="float: right;margin-top: 20px;">
        	<a href="<?php echo base_url() ?>" style="text-decoration: none;">< Kembali ke beranda</a>
        </h3>
        <div  style="clear: both;"></div>
        </div>
        
    </div>
</body>
</html>