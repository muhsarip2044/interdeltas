<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <img src="<?php echo base_url("assets/frontend/images/".$division->image) ?>">
            <h1><?php echo $division->name ?></h1>
            <p>
                <?php echo $division->description ?>
            </p>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="list-group">
                    <h4>Category</h4>
                    <?php 
                    if (count($division->category)>0) {
                        foreach ($division->category as $key) {
                            echo '
                            <a href="'.base_url($key->url_prefix.$division->slug.'/'.$key->slug).'" class="list-group-item">'.$key->name.'</a>
                            ';
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-9">
                <h4>Best Product</h4>
                <div class="row">
                    <?php 
                    if (count($division->feature)>0) {
                        foreach ($division->feature as $key) {
                            echo '
                            <div class="col-lg-3">
                                <a  href="'.base_url($key->product->url_prefix).'">
                                <img class="img img-responsive" src="'.base_url($key->product->image_path.$key->product->image).'">
                                '.$key->product->name.' <br>
                                Description : '.$key->product->description.'
                                </a> 
                            </div>
                            ';
                        }
                    }
                    ?>
                    
                </div>
            </div>
        </div>
    </div>  
</div>

