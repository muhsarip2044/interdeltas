<div class="container-fluid">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php 
            if (count($slide)>0) {
                foreach ($slide as $key ) {
                    $url = "";
                    if ($key->type==1) {
                        $url = "href='".base_url("divisions/".$key->division->slug)."'";
                    }

                    echo '
                    <div class="swiper-slide"  >
                        <img src="'.base_url('assets/frontend/images/slides/'.$key->image).'" style="width: 100%;height: 450px;">
                        <div class="swiper-content">
                            <a '.$url.' style="text-decoration:none"> 
                                <div class="swiper-title">
                                    '.$key->name.'
                                </div>
                                <div class="swiper-description">
                                    '.$key->description.'
                                </div>
                            </a>
                        </div>
                    </div>
                    ';
                }
            }
            ?>
            
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next" style="background-image: none;color: #f4f4f4;font-size: 30px">
            <i class="fa fa-chevron-circle-right "></i>
        </div>
        <div class="swiper-button-prev" style="background-image: none;color: #f4f4f4;font-size: 30px">
            <i class="fa fa-chevron-circle-left "></i>
        </div>
    </div>    
</div>

