<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-lg-3">
                <div class="list-group">
                    <h4>Category</h4>
                    <?php 
                    if (count($division->category)>0) {
                        foreach ($division->category as $key) {
                            echo '
                            <a href="'.base_url($key->url_prefix.$division->slug."/".$key->slug).'" class="list-group-item">'.$key->name.'</a>
                            ';
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-9">
                <h4>Kategori <?php echo $cat->name ?></h4>
                <p>
                    <?php echo $cat->description; ?>
                </p>
                <div class="row">
                    <?php 
                    if (count($products)>0) {
                        foreach ($products as $key) {
                            echo '
                            <div class="col-lg-3">
                                <a  href="'.base_url($key->url_prefix).'">
                                <img class="img img-responsive" src="'.base_url($key->image_path."medium_".$key->image).'">
                                '.$key->name.' <br>
                                Description : '.$key->description.'
                                </a> 
                            </div>
                            ';
                        }
                    }else{
                        echo '
                        <div class="col-lg-12">
                            <div class="alert alert-info">
                                Maaf produk dalam kategori ini tidak tersedia.
                            </div>
                        </div>
                        ';
                    }
                    ?>
                    <!-- Pagination -->
                    <div class="pagination">
                        <ul>
                           <!--  <li>
                                <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a>
                            </li> -->
                            <?php 
                            $num = 0;
                            for ($i=0; $i < $num_page ; $i++) { 
                                
                                
                                $display_num = $i+1;

                                if ($i == 0) {
                                    if (empty($page)) {
                                        $active = "active";
                                    }else{
                                        $active = "";
                                    }

                                    echo '<li><a href="'.base_url($path_url).'">'.$display_num.'</a> </li>';
                                }else{
                                    $num +=$per_page;
                                    if ($num == $page) {
                                        $active = "active";
                                        echo '<li><a style="color:#ef9e10;">'.$display_num.'</a> </li>';
                                    }else{
                                        $active = '';
                                        echo '<li><a href="'.base_url($path_url).'?page='.$num.'">'.$display_num.'</a> </li>';
                                    }
                                }
                                
                            }
                            ?>
                           <!--  <li>
                                <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a>
                            </li> -->
                        </ul>
                    </div>
                    <!-- end: Pagination -->
                    
                </div>
            </div>
        </div>
    </div>  
</div>

