<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-lg-6">
                <img class="img img-responsive"  src="<?php echo base_url($product->image_path.$product->image) ?>">
            </div>
            <div class="col-lg-6">
                <h1><?php echo $product->name ?></h1>
                <h4><strong>Deskripsi Produk :</strong></h4>
                <p><?php echo $product->description ?></p>
            </div>
        </div>
    </div>  
</div>

