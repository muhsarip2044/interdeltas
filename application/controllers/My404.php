<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index(){
		//$this->output->set_status_header('404');

		$data['title']  = "Halaman yang anda tuju tidak ditemukan.";
		$data['content']  = $this->load->view($this->folder_view_frontend.'page/my404', $data, true);
		$this->load->view($this->folder_view_frontend.'master_feature',$data);
	}

}

/* End of file My404.php */
/* Location: ./application/controllers/My404.php */