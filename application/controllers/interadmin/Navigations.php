<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigations extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_menus');
    	$crud->set_subject('Navigasi / Menu Website');
    	$crud->where("parent_id",null);
    	$crud->where("division_id",null);
    	// column
    	$crud->columns('name','en_name','child');

    	// field
		$crud->edit_fields('name','en_name');

		//aliasing 
		$crud->display_as("name","Menu Label");
		$crud->display_as("en_name","<i style='color:blue'>English : Menu Label</i>");

		// relation
		//$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');

		// validation
		$crud->required_fields('name',"en_name");

		$crud->callback_column('child',array($this,'callback_column_child'));

		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_read();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Navigasi / Menu Website';
		$data['crumb'] = array( 'Navigasi / Menu Website' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    public function callback_column_child($value, $row)
	{
		$checkMenu  = $this->db->where("parent_id",$row->id)->get("tb_menus");
		if ($checkMenu->num_rows()>0) {
			return "<a href='".base_url($this->ADMIN_NAMESPACE."/navigations/child/".$row->id)."' class='btn btn-primary btn-xs'>Manage child menu</a>";	
		}else{
			return "<button class='btn btn-default btn-xs disabled'>Tidak ada child menu</button>";
		}
		
	}
	public function child($parent_id=null)
    {
    	$checkMenu = $this->db->select("name")->where("id",$parent_id)->get("tb_menus");

    	if ($checkMenu->num_rows() == 0) {
    		redirect(base_url($this->ADMIN_NAMESPACE."/navigations"),'refresh');
    	}
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_menus');
    	$crud->set_subject('Navigasi / Menu Website : '.$checkMenu->row()->name);
    	$crud->where("parent_id",$parent_id);

    	// column
    	$crud->columns('name','en_name');

    	// field
		$crud->edit_fields('name','en_name');

		//aliasing 
		$crud->display_as("name","Menu Label");
		$crud->display_as("en_name","<i style='color:blue'>English : Menu Label</i>");

		// relation
		//$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');

		// validation
		$crud->required_fields('name',"en_name");

		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_read();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Navigasi / Menu Website : '.$checkMenu->row()->name;
		$data['crumb'] = array( 
			'Navigasi / Menu Website' => $this->ADMIN_NAMESPACE.'/navigations',
			$checkMenu->row()->name => '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
}

/* End of file Divisions.php */
/* Location: ./application/controllers/Interadmin/Divisions.php */