<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_information extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    public function index(){
    	// set division id
    	switch ($this->userGroup) {
    		case '5':
    			$division_id = 1;
    			break;
    		case '6':
    			$division_id = 2;
    			break;
    		case '7':
    			$division_id = 3;
    			break;
    		
    		default:
    			$division_id = 1;
    			break;
    	}
    	redirect(base_url($this->ADMIN_NAMESPACE."/Division_information/information/edit/".$division_id),'refresh');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function information()
    {
    	$crud = new grocery_CRUD();


    	$crud->set_table('tb_divisions');
    	$crud->set_subject('Informasi');

    	// column
    	$crud->columns('name','description','phone_extension',"slug");

    	// field
		$crud->edit_fields('name','description','phone_extension',"slug");

		//aliasing 

		// relation
		//$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');

		// validation
		$crud->required_fields('name','description','phone_extension',"slug");

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));
		$crud->callback_before_update(array($this,'callback_slug'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_read();

		//VIEW
		$output = $crud->render();

		$data['script'] = '
		$("#save-and-go-back-button").remove();
		';

		$data['judul'] = 'Edit Informasi';
		$data['crumb'] = array(
			'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
			'Edit Informasi' => '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
}