<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Division_dashboard extends ADMIN_Controller { 
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
 	
    public function index()
    {
    	$this->load->model('admin_menu');
		$widget = array(
			(object)array(
				'url' 	=> 'division_information',
				'icon' 	=> 'file',
				'label'	=> 'Informasi Umum Division'
			),
			(object)array(
				'url' 	=> 'division_slide',
				'icon' 	=> 'file',
				'label'	=>  'Slide Promo'
			),
			(object)array(
				'url' 	=> 'division_product',
				'icon' 	=> 'file',
				'label'	=>  'Produk'
			),
			(object)array(
				'url' 	=> 'division_product_category',
				'icon' 	=> 'file',
				'label'	=>  'Kategori Produk'
			),
			(object)array(
				'url' 	=> 'division_brand',
				'icon' 	=> 'file',
				'label'	=>  'Brand/Merk Produk'
			),
			(object)array(
				'url' 	=> 'division_catalogue',
				'icon' 	=> 'file',
				'label'	=>  'Katalog Produk'
			),
			(object)array(
				'url' 	=> 'product_supports',
				'icon' 	=> 'file',
				'label'	=>  'Dukungan Produk'
			),
			// (object)array(
			// 	'url' 	=> ,
			// 	'icon' 	=> ,
			// 	'label'	=> 
			// ),
		);

		$output = (object)array('data' => '','widget' => $widget,'output' => '' , 'js_files' => null , 'css_files' => null);
		
		$data['judul'] = 'Dashboard';

		$template = 'master';
		$view = 'backend/division_dashboard';
		$this->outputview->output_admin($view, $template, $data, $output);
	}
}