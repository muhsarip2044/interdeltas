<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisions extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_divisions');
    	$crud->set_subject('Divisi Perusahaan');

    	// column
    	$crud->columns('name','description','phone_extension',"slug");

    	// field
		$crud->edit_fields('name','description','slideshow','phone_extension',"slug");

		//aliasing 
		$crud->display_as("slideshow","Slide Promo");

		// relation
		//$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');

		// validation
		$crud->required_fields('name','description','phone_extension',"slug");

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));
		$crud->callback_before_update(array($this,'callback_slug'));
		$crud->callback_field('slideshow',array($this,'callback_column_slideshow'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_read();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Divisi Perusahaan';
		$data['crumb'] = array( 'Divisi Perusahaan' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    public function callback_column_slideshow($value = '', $primary_key = null)
	{
		return '<a href="'.base_url($this->ADMIN_NAMESPACE.'/slideshows/division/'.$primary_key).'" class="btn btn-primary btn-lg btn-block"><i class="fa fa-chevron-right"></i> Edit Slide Promo</a>';
	}

}

/* End of file Divisions.php */
/* Location: ./application/controllers/Interadmin/Divisions.php */