<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_product extends ADMIN_Controller {
 
    function __construct() 
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    public function index(){
    	// set division id
    	switch ($this->userGroup) {
    		case '5':
    			$division_id = 1;
    			break;
    		case '6':
    			$division_id = 2;
    			break;
    		case '7':
    			$division_id = 3;
    			break;
    		
    		default:
    			$division_id = 1;
    			break;
    	}
    	redirect(base_url($this->ADMIN_NAMESPACE."/division_product/data/".$division_id),'refresh');
    }

    /*
	* Menampilkan daftar produk
    */
    public function data($division_id)
    {
    	if ($division_id == null) {
    		redirect($this->ADMIN_NAMESPACE.'/division_product','refresh');
    	}

    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_products');
    	$crud->set_subject('Daftar Produk');
    	$crud->where("tb_products.division_id",$division_id);

    	// column
    	$crud->columns('name','image','category_id','brand_id','tag','status');

    	// field
		$crud->add_fields('name','division_id','category_id','brand_id','spec','image','video_type','youtube_source','local_source','description','additional_info','reviews','slug','tag','status');
		$crud->edit_fields('name','category_id','brand_id','spec','image','video_type','youtube_source','local_source','description','additional_info','reviews','slug','tag','status');

		// relation
		//
		
		$crud->set_relation('brand_id', 'tb_brand_products', 'name');
		$crud->set_relation_n_n('tag', 'tb_product_tags', 'tb_tags', 'product_id', 'tag_id', 'name');

		// aliasing
		$crud->display_as("spec","Spesifikasi");
		$crud->display_as("category_id","Kategori");
		$crud->display_as("brand_id","Brand/Merk");
		$crud->display_as("name","Nama Produk");
		$crud->display_as("youtube_source","Kode Embed/Semat Youtube");
		$crud->display_as("local_source","Video Product");
		$crud->display_as("description","Deskripsi Product");
		$crud->display_as("status","Status Publikasi");

		// validation
		$crud->required_fields('name','category_id','brand_id','video_type','image','description','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/products');
   		$crud->set_field_upload('local_source','assets/frontend/videos/products');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type("division_id","hidden",$division_id);
		
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));
		$crud->callback_before_update(array($this,'callback_slug'));

		// set nilai resize
		$this->resize_size_image = array(
	        array(265,240,"medium_"),
	        array(1000,800,"large_"),
	      );
		$this->resize_min_size = 2000; // minimal file yg di upload 2 kb
		$this->resize_max_size = 2000000; // maximal size file yang di upload 2 mb

		$crud->callback_after_upload(array($this,'callback_after_upload'));
		$crud->callback_before_upload(array($this,'callback_before_upload'));

		$crud->callback_after_insert(array($this, 'callback_after_insert'));
		$crud->callback_after_update(array($this, 'callback_after_update'));

		$crud->callback_before_insert(array($this,'callback_before_save'));
		$crud->callback_before_update(array($this,'callback_before_save'));

		$crud->callback_before_delete(array($this,'callback_before_delete'));

		$crud->callback_field('spec',array($this,'callback_field_spec'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		$data['crumb'] = array( 'Daftar Produk' => '' );

		

		// State
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();

		$data['script'] = "";

		if($state == 'list'){
			$data['crumb'] = array( 
				'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
				'Daftar Produk' => '' 
			);
			$crud->set_relation('category_id', 'tb_category_products', 'name');
			$crud->set_relation('division_id', 'tb_divisions', 'name');
		}
		elseif($state == 'add'){
			// Title header
			$data['script'] = '
			$(".box-title").html("Tambah Produk");
			';
			if (empty($division_id)) {
				// Redirect ke halaman pilih divisi jia var get division_id kosong
				redirect(base_url($this->ADMIN_NAMESPACE."/products/choose_division"),'refresh');
			}else{
				// get info divisi
				$division = $this->db->select("name")->where("id",$division_id)->get("tb_divisions",1)->row();

				// Set relasi agar terfilter sesuai divisi id
				$crud->set_relation('category_id', 'tb_category_products', 'name',array('division_id' => $division_id));
				
				// Membuat input hidden divisi id sesuai divisi id
				$crud->field_type("division_id","hidden",$division_id);

				// Set Breadcrumb
				$data['crumb'] = array( 
					'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
					'Daftar Produk' => $this->ADMIN_NAMESPACE.'/division_product/data/'.$division_id ,
					'Tambah Produk ' => ''
				);
			}



			
		}elseif($state == 'edit'){
			// get current data
			$cur_data = $this->db->where("id",$this->uri->segment(5))->get("tb_products");

			// Title header
			$data['script'] = '
			$(".box-title").html("Edit Produk");
			';
			$data['crumb'] = array( 
				'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
				'Daftar Produk' => $this->ADMIN_NAMESPACE.'/division_product/data/'.$division_id ,
				'Edit Produk' => ''
			);
			$crud->set_relation('category_id', 'tb_category_products', 'name',array('division_id' => $division_id));

		}else{

			//$crud->set_relation('category_id', 'tb_category_products', 'name',array('division_id' => $cur_data->row()->division_id));
			$crud->field_type("division_id","hidden",$this->uri->segment(5));
		}


		$crud->unset_read();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Daftar Produk';
		

		$data['script'] .= '
		

		$("#youtube_source_field_box").hide();
		$("#local_source_field_box").hide();
		var val = $("#field-video_type").val();

		if(val == "Youtube"){
			$("#youtube_source_field_box").show();
		}else{
			$("#local_source_field_box").show();
		}

		$("#field-video_type").on("change",function(){
			var val = $("#field-video_type").val();
			if(val == "None"){
				$("#youtube_source_field_box").hide();
				$("#local_source_field_box").hide();

			}else if(val == "Youtube"){
				$("#youtube_source_field_box").show();
				$("#local_source_field_box").hide();

				$("#alert-info-youtube").remove();
				$("#youtube_source_input_box").append("<div id=\"alert-info-youtube\"><br><div class=\"alert alert-info\">Anda telah memilih sumber youtube sebagai video produk. contoh Kode Embed/Semat : <strong><br>&lt;iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/laERiO42h94\" frameborder=\"0\" allowfullscreen&gt;&lt;/iframe&gt;</strong></div></div>");
			}else{
				$("#local_source_field_box").show();
				$("#youtube_source_field_box").hide();

				$("#alert-info-local").remove();
				$("#local_source_input_box").append("<div id=\"alert-info-local\"><br><div class=\"alert alert-info\">Anda telah memilih sumber penyimapanan lokal sebagai video produk. Format video yang diizinkan adalah MP4.</div></div>");
			}
		});
		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    public function callback_field_spec($value = '', $primary_key = null){
		return '
		<div id="spec-area" class="row"><div class="col-lg-12"><h5>Silahkan pilih kategori terlebih dahulu.</h5></div></div>
		<script>
		$("#field-category_id").on("change",function(){
			var cat_id = $(this).val();
			$("#alertCategory").remove();
			$.getJSON("'.base_url($this->ADMIN_NAMESPACE."/products/ajax_get_option").'/" + cat_id, function(result){
		        var html = "";
		        var res_len = result.length;
		        if (res_len>0) {
		        	
		        	for(var i = 0 ; i < res_len; i++){
		        		var spec = "";
		        		if(result[i].spec.length >0){
		        			for(var e =0; e < result[i].spec.length; e++){
		        				var ch = result[i].spec;
		        				spec += "<option value=\'" + ch[e].id + "\'>" +  ch[e].name + " </option>";
		        				
		        			}
		        		}
						html += "<div class=\"col-lg-4\">";
		        		html += result[i].option;
		        		html += "<select class=\"form-control \" id=\"option-" + result[i].option + "\" name=\"spec[]\" style=\"width:100%;\">" + spec + "</select>";
		        		html += "</div>";

		        		
		        	}
		        	$("#spec-area").html("");	
		        }else{
		        	$("#spec-area").html("<div class=\"col-lg-12\"><div class=\"alert alert-warning\">Tidak ada spesifikasi produk pada kategori ini. </div></div>");
		        }
		        html += "<div class=\"col-lg-12 text-left\"><br><a href=\"#\">Klik disini</a> untuk menambah/merubah parameter spesifikasi produk.</div>";
		        $("#spec-area").append(html);
		        
		    }).fail(function(jqXHR, textStatus, errorThrown) { 
		    	$("#category_id_input_box").append("<div id=\"alertCategory\" class=\"alert alert-warning\">Please try again .</div>");
		    
		    });
			
			//$("#spec-area").html("<select class=\"chosen-select \" id=\"spec-example\" style=\"width:100%;\"><option value=\"tes\">tes</option></select>");
			//$("#spec-example").chosen({});
			//$(".chosen-select ").css("min-width","100px");
		});

		</script>
		';
	}
	/*
	* Callback before delete
	* Menghapus record yang berelasi dengan product
	*/
	public function callback_before_delete($primary_key){
		$this->db->where('product_id', $primary_key);
		$this->db->delete('tb_product_specs');

		$this->db->where('product_id', $primary_key);
		$this->db->delete('tb_product_features');

		$this->db->where('product_id', $primary_key);
		$this->db->delete('tb_product_tags');

		return true;
	}
	/*
	* Callback before save
	* convert spec variable to string
	*/
	public function callback_before_save($post_array){
		if (isset($post_array['spec'])) {
			$post_array['spec'] = implode("-", $post_array['spec']);
		}

		$post_array = $this->callback_slug($post_array);

		return $post_array;
	}


	/*
	* Get spec
	* Ajax request untuk mendapatkan opsi dan spec sesuai kategori id
	*/
	public function ajax_get_option($category_id=null){
		if ($category_id != '') {
			$array_result = array();
			$data = $this->db->select("*")->where("category_id",$category_id)->get("tb_category_options");
			if ($data->num_rows()>0) {
				foreach ($data->result() as $key ) {
					$check_spec = $this->db->select("*")->where("option_id",$key->id)->get("tb_category_option_specs");
					if ($check_spec->num_rows()>0) {
						array_push($array_result, (object)array('option' => $key->name , 'spec' => $check_spec->result() ));
					}
				}
				echo json_encode($array_result);
			}else{
				echo json_encode($array_result);
			}
		}
	}

	/*
	* Callback after insert
	* Insert spec product yang dipilih
	*/
	public function callback_after_insert($post_array,$primary_key){

		if (isset($post_array['spec'])) {
			$post_array['spec'] = explode("-", $post_array['spec']);
			if (is_array($post_array['spec'])) {
				if (count($post_array['spec'])>0) {
					for($i=0 ; $i < count($post_array['spec']) ; $i++){

						$object = array(
							'product_id'	=> $primary_key,
							'spec_id'		=> $post_array['spec'][$i]['spec']
						);
						$this->db->insert('tb_product_specs', $object);
					}
				}
			}
		}
		
		 unset($post_array['spec']);
		return true;
	}

	/*
	* Callback after update
	* Hapus spec lama, Insert spec product yang dipilih
	*/
	public function callback_after_update($post_array,$primary_key){
		if (isset($post_array['spec'])) {
			$post_array['spec'] = explode("-", $post_array['spec']);

			// Hapus spec lama
			$this->db->where('product_id', $primary_key);
			$this->db->delete('tb_product_specs');
			if (is_array($post_array['spec'])) {
				if (count($post_array['spec'])>0) {
					for($i=0 ; $i < count($post_array['spec']) ; $i++){

						$object = array(
							'product_id'	=> $primary_key,
							'spec_id'		=> $post_array['spec'][$i]['spec']
						);
						$this->db->insert('tb_product_specs', $object);
					}
				}
			}
		}
		
		  unset($post_array['spec']);
		return true;
	}
}

/* End of file Products.php */
/* Location: ./application/controllers/Interadmin/Products.php */