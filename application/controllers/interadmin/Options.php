<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function index()
    {
    	
    	$crud = new grocery_CRUD();

    	

 
    	$crud->set_table('tb_category_options');
    	
    	

    	// column
    	$crud->columns('name','value');

 		

		// aliasing
		$crud->display_as("division_id","Divisi");
		$crud->display_as("name","Nama Parameter");

		

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_column('value',array($this,'callback_column_value'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		// script addon
		$data['script'] = '
	
						
		';

		$data['crumb'] = array( 
			'Kategori Produk'	=> $this->ADMIN_NAMESPACE.'/product_categories',
			'Specification Parameter' => '' 
		);

		// State
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		

		if($state == 'list'){


			if (empty($this->input->get('category_id'))) {
	    		redirect(base_url($this->ADMIN_NAMESPACE."/product_categories"),'refresh');
	    		die();
	    	}
	    	// Get data category
	    	$data_category = $this->db->select("*")->where("id",$this->input->get('category_id'))->get("tb_category_products");

	    	// check apakah category_id valid
	    	if ($data_category->num_rows() == 0) {
	    		redirect(base_url($this->ADMIN_NAMESPACE."/product_categories"),'refresh');
	    		die();
	    	}


			$crud->set_subject('Specification Parameter Category : '.$data_category->row()->name);
			$crud->where("category_id",$this->input->get('category_id'));

			// add category id to link insert
			$data['script'] = '
			var href = $(".add_button").attr("href");
			$(".add_button").attr("href", href + "?category_id='.$this->input->get('category_id').'");
			';
		}elseif($state == 'add'){
			$crud->set_subject('Specification Parameter');
			// field
			$crud->fields('category_id','name');
			$crud->field_type("category_id","hidden",$this->input->get('category_id'));

			// add category id to link insert
			$data['script'] = '
			
			$("#save-and-go-back-button").hide();
			';
			
		}elseif($state == 'edit'){
			// get category id
			$current_data = $this->db->where("id",$this->uri->segment(5))->get("tb_category_options");


			$crud->set_subject('Specification Parameter');
			// field
			$crud->fields('name');

			// add category id to link insert
			$data['script'] = '
			$(".form-horizontal").append("<a href=\"'.base_url($this->ADMIN_NAMESPACE."/options?category_id=".$current_data->row()->category_id).'\" class=\"btn btn-default\">Back</a>");
			$("#cancel-button").hide();
			$("#save-and-go-back-button").hide();
			';
				
		}elseif($state == 'update_validation'){
			$crud->required_fields('name');
		}elseif($state == "insert_validation"){
			$crud->required_fields('name','category_id');
		}else{
			$crud->set_subject('Specification Parameter');
		}

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Kategori Produk';
		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

    /*
	* Tabel SPecification
	* @param int option_id
    */
    public function spec($option_id=null)
    {
    	// check valid option_id
    	$check_option = $this->db->where("id",$option_id)->get("tb_category_options");
    	if ($check_option->num_rows() == 0) {
    		redirect(base_url($this->ADMIN_NAMESPACE."/product_categories"),'refresh');
	    	die();
    	}

    	$crud = new grocery_CRUD();

    	

 
    	$crud->set_table('tb_category_option_specs');
    	
    	$crud->where("option_id",$option_id);

    	// column
    	$crud->columns('name');

 		

		// aliasing
		$crud->display_as("option_id","Nama Parameter");
		$crud->display_as("name","Nilai Parameter");

		

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		// script addon
		$data['script'] = '
	
						
		';

		$data['crumb'] = array( 
			'Kategori Produk'	=> $this->ADMIN_NAMESPACE.'/product_categories',
			'Specification Parameter' => $this->ADMIN_NAMESPACE.'/options?category_id='.$check_option->row()->category_id,
			'Nilai Parameter' => '' 
		);

		// State
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		

		if($state == 'list'){

		}elseif($state == 'add'){
			$crud->fields("name","option_id");
			$crud->field_type("option_id","hidden",$option_id);
		}elseif($state == 'edit'){
			$crud->fields("name");
				
		}elseif($state == 'update_validation'){
			$crud->required_fields('name');
		}elseif($state == "insert_validation"){
			$crud->required_fields('name','option_id');
		}else{
			
		}

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Nilai Parameter';
		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    /*
	* Menampilkan value parameter dan link edit
    */
    public function callback_column_value($value, $row){
    	$res_html = "";

    	$check_value = $this->db->where("option_id",$row->id)->get("tb_category_option_specs");
    	if ($check_value->num_rows()>0) {
    		$res_html .= "Terdapat ".$check_value->num_rows()." nilai :<ol>";
    		foreach ($check_value->result() as $key) {
    			$res_html .= '<li>'.$key->name.'</li>';
    		}
    		$res_html .= '</ol>
    		<br>
    		<a href="'.base_url($this->ADMIN_NAMESPACE."/options/spec/".$row->id).'" class="btn btn-warning">Klik ini untuk menambah/mengatur nilai parameter</a>
    		';
    	}else{
    		$res_html .='Tidak ditemukan nilai untuk parameter ini. <br>
    		<a href="'.base_url($this->ADMIN_NAMESPACE."/options/spec/".$row->id).'" class="btn btn-warning">Klik ini untuk menambah/mengatur nilai parameter</a>
    		';
    	}

    	return $res_html;

    }

}

/* End of file Categories.php */
/* Location: ./application/controllers/Interadmin/Categories.php */