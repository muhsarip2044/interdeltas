<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_brand extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_brand_products');
    	$crud->set_subject('Brand Product');

    	// column
    	$crud->columns('name','image');

    	// fields
		$crud->fields('name','image');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("name","Daerah / Kota");
		$crud->display_as("image","Icon");

		// validation
		$crud->required_fields('name','image');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images');

		//FIELD TYPES
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Brand Product';
		$data['crumb'] = array( 
			'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
			'Brand Product' => '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
}

/* End of file Offices.php */
/* Location: ./application/controllers/Interadmin/Offices.php */