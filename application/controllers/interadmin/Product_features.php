<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_features extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan produk feature ber relasi dengan tb_products
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_product_features');
    	$crud->set_subject('Featuring Product');

    	// column
    	$crud->columns('product_id','updated_at');

    	// field
		$crud->fields('product_id','updated_at');

		// relation
		$crud->set_relation('product_id', 'tb_products', 'name');

		// aliasing
		$crud->display_as("product_id","Nama Product");
		$crud->display_as("updated_at","Tanggal Ubah");

		// validation
		$crud->required_fields('product_id','updated_at');

   		// field upload
   		//$crud->set_field_upload('image','assets/frontend/images/reports');
   		//$crud->set_field_upload('file','assets/frontend/files/reports');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type('updated_at','hidden', date("Y-m-d h:i:s",time()));

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();
		$crud->unset_read();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Featuring Product';
		$data['crumb'] = array( 
			'Konten Homepage'		=> $this->ADMIN_NAMESPACE.'/homepages',
			'Featuring Product' 	=> '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
}

/* End of file Kodak_express.php */
/* Location: ./application/controllers/Interadmin/Kodak_express.php */