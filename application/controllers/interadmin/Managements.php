<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Managements extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan data komisaris
    */
    public function komisaris()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_managements');
    	$crud->set_subject('Data Komisaris');
    	$crud->where("type",1);

    	// column
    	$crud->columns('name','position','image','sort');

    	// field
		$crud->add_fields('name','type','position','image','sort');
		$crud->edit_fields('name','position','image','sort');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Nama Lengkap");
		$crud->display_as("position","Jabatan Lengkap");
		$crud->display_as("image","Photo");
		$crud->display_as("sort","Urutan Tampilan");

		// validation
		$crud->required_fields('name','position','image');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/managements');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type('type', 'hidden',1);

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Data Komisaris';
		$data['crumb'] = array( 'Data Komisaris' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

    /*
	* Menampilkan data direksi
    */
    public function direksi()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_managements');
    	$crud->set_subject('Data Direksi');
    	$crud->where("type",2);

    	// column
    	$crud->columns('name','position','image');

    	// field
		$crud->add_fields('name','type','position','image','sort');
		$crud->edit_fields('name','position','image','sort');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Nama Lengkap");
		$crud->display_as("position","Jabatan Lengkap");
		$crud->display_as("image","Photo");
		$crud->display_as("sort","Urutan Tampilan");

		// validation
		$crud->required_fields('name','position','image');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/managements');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type('type', 'hidden',1);

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Data Direksi';
		$data['crumb'] = array( 'Data Direksi' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Managements.php */
/* Location: ./application/controllers/Interadmin/Managements.php */