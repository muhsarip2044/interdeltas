<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservations extends ADMIN_Controller {
	function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    /*
	* Menampilkan data master trip berikut CRUD nya
    */
	public function master()
	{
		$crud = new grocery_CRUD();

    	$crud->set_table('tb_invoices');
    	$crud->set_subject('Master Reservasi');
    	$crud->columns('full_name','email','phone','trips','status','total_charge');

    	$crud->set_relation_n_n('trips', 'tb_reservations', 'tb_trips', 'invoice_id', 'trip_id', 'name');
    	
    	$crud->fields('full_name','email','phone','trips','status');
    	//$crud->add_fields('name','company','address','phone','image','status');
		//$crud->edit_fields('name','company','address','phone','image','status');

		// aliasing
		$crud->display_as("trips","Paket Wisata");

		// calbback
		$crud->callback_column('status',array($this,'_callback_column_status'));
		$crud->callback_column('total_charge',array($this,'_callback_column_total_charge'));

		// add action


		// override tampilan 
		$state = $crud->getState();
		switch ($state) {
			case 'list':
				$script = '$(".add_button").html("Buat Reservasi Baru");'; 
				break;
			case 'add':
				$script = '$(".box-title").html("Form Reservasi Baru").css("font-size","25px");'; 
				break;
			
			default:
				$script = '';
				break;
		}


		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Master Reservasi';
		$data['crumb'] = array( 'Master Reservasi' => '' );

		

		$data['script'] = $script;
		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
	}
	/*
	* Menampilkan reservasi terbaru
    */
	public function new_reservation()
	{
		$crud = new grocery_CRUD();

    	$crud->set_table('tb_invoices');
    	$crud->where("tb_invoices.status",0);
    	$crud->set_subject('Master Reservasi');
    	$crud->columns('full_name','email','phone','trips','status','total_charge');

    	$crud->set_relation_n_n('trips', 'tb_reservations', 'tb_trips', 'invoice_id', 'trip_id', 'name');
    	
    	$crud->fields('full_name','email','phone','trips','status');
    	//$crud->add_fields('name','company','address','phone','image','status');
		//$crud->edit_fields('name','company','address','phone','image','status');

		// aliasing
		$crud->display_as("trips","Paket Wisata");

		// calbback
		$crud->callback_column('status',array($this,'_callback_column_status'));
		$crud->callback_column('total_charge',array($this,'_callback_column_total_charge'));

		// add action
		$crud->add_action('Konfirmasi', '', base_url($this->ADMIN_NAMESPACE.'/reservations/confirm_reservation/'),'ui-icon-plus disabled');


		// override tampilan 
		$state = $crud->getState();
		switch ($state) {
			case 'list':
				$script = '$(".add_button").html("<i class=\'fa fa-plus\'></i> Buat Reservasi Baru");'; 
				break;
			case 'add':
				$script = '$(".box-title").html("Form Reservasi Baru").css("font-size","25px");'; 
				break;
			
			default:
				$script = '';
				break;
		}


		// Menambahkan tulisan jumlah reservasi
		$this->load->model('invoice');
		$invoice = Invoice::where("status",0)->count();

		$script .= '
		$(".tDiv2").append("<div class=\'alert alert-success\' style=\'margin-top:10px;\'>Total reservasi yang belum di konfirmasi ada <strong>'.$invoice.' Reservasi</strong></div>");
		';


		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Master Reservasi';
		$data['crumb'] = array( 'Master Reservasi' => '' );

		

		$data['script'] = $script;
		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
	}

	/*
	* Menampilkan halaman untuk konfirmasi reservasi bart
	* @param integer invoice_id
	* @return string
	*/
	public function confirm_reservation($invoice_id)
	{
		// cek validasi invoice ID
		$this->load->model('invoice');
		$this->load->model('reservation');
		$this->load->model('trip');
		$invoice = Invoice::where("id",$invoice_id)->with(['reservation' => function($q) {
	        $q->with('trip');
	    }])->first();

		// Lempar pesan kesalahan apabila
		if (count($invoice)==0) {
			
			show_404();
			die();
		}

		$output = (object)array(
			'data' => '',
			'invoice' => $invoice,  
			'url_back'	=> $this->ADMIN_NAMESPACE."/reservations/new_reservation",
			'output' => '' , 
			'js_files' => null , 
			'css_files' => null
		);
		
		$data['judul'] = 'Konfirmasi Reservasi';

		$template = 'master';
		$view = 'backend/confirm_reservation';
		$this->outputview->output_admin($view, $template, $data, $output);
	}

	public function send_payment_link($id)
	{
		// init model invoice
		$this->load->model('invoice');
		$this->load->model('reservation');
		$this->load->model('trip');

		$invoice = Invoice::where("id",	$id)->with(['reservation' => function($q) {
	        $q->with('trip');
	    }])->first();

		if (count($invoice)==0) {
			echo "Invoice tidak valid";
			die();
		}

		$this->load->library('notification_helper');
		$send = $this->notification_helper->send_link_payment($invoice);
		if ($send) {
			// email berhasil dikirim, kembalikan respon sukses
			echo json_encode(array(
				"success"	=> true,
				"message"	=> "Berhasil mengirim link pembayaran"
			));	
		}else{
			// email gagal terkirim, kembalikan respon untuk mengulang
			echo json_encode(array(
				"success"	=> false,
				"message"	=> "Pengiriman link gagal, harap di ulang kembali."
			));
		}
	}

	/*
	* Menampilkan tex status invoice
	*/
	public function _callback_column_status($value, $row)
	{
	  switch ($value) {
            case 0:
                return '<div class="alert alert-info">Menunggu Konfirmasi Paket</info>';
                break;
            case 1:
                return '<div class="alert alert-warning">Menunggu Pembayaran</div>';
                break;
            case 2:
                return '<div class="alert alert-success">Pembayaran Lunas</div>';
                break;
            case 3:
                return '<div class="alert alert-danger">Invoice Dibatalkan system</div>';
                break;
            case 4:
                return '<div class="alert alert-info">Invoice menunggu reservasi custom ditinjau</div>';
                break;
            case 5:
                return '<div class="alert alert-info">Invoice menunggu pengajuan reservasi custom di setujui customer</div>';
                break;
            default:
                return "Invoice tidak valid";
                break;
        }
	}
	public function _callback_column_total_charge($value, $row)
	{
		return 'Rp. '.number_format($value);
	}

}

/* End of file Reservations.php */
/* Location: ./application/controllers/Travadmin/Reservations.php */