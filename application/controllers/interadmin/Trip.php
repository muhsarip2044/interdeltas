<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trip extends ADMIN_Controller {
	function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    /*
	* Menampilkan data master trip berikut CRUD nya
    */
	public function master()
	{
		$crud = new grocery_CRUD();

    	$crud->set_table('tb_trips');
    	$crud->set_subject('Master Data Trip');
    	$crud->columns('name','duration','price','discount','agent_id','country_id','city_id');
    	
    	$crud->add_fields('name','duration','price','discount','agent_id','country_id','city_id');
		$crud->edit_fields('name','duration','price','discount','agent_id','country_id','city_id');

		// aliasing
		$crud->display_as("agent_id","Agent");
		$crud->display_as("country_id","Country");
		$crud->display_as("city_id","City");

		// relation table
		$crud->set_relation("agent_id","tb_agents","name");
		$crud->set_relation("country_id","tb_countries","name");
		$crud->set_relation("city_id","tb_cities","name");

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Master Data Trip';
		$data['crumb'] = array( 'Master Data Trip' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
	}

}

/* End of file Trip.php */
/* Location: ./application/controllers/Travadmin/Trip.php */