<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshows extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan kontent slideshow
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_slides');
    	$crud->set_subject('Konten Slideshow Promo');
    	$crud->where("type",1);

    	// column
    	$crud->columns('name','image','description','status');

    	// field
		$crud->fields('name','en_name','image','description','en_description','status');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Judul Slide");
		$crud->display_as("description","Keterangan Slide");
		$crud->display_as("image","Gambar Background");
		$crud->display_as("status","Tampilkan");
		$crud->display_as("en_name","<i style='color:blue'>English : Judul Slide</i>");
		$crud->display_as("en_description","<i style='color:blue'>English : Keterangan Slide</i>");

		// validation
		$crud->required_fields('name','image','description','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/slides');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		//$crud->callback_before_insert(array($this,'callback_slug'));
		//$crud->callback_before_update(array($this,'callback_slug'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		$crud->unset_texteditor('description');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Konten Slideshow Promo';
		$data['crumb'] = array( 
			'Konten Homepage' => 'interadmin/homepages/index/edit/1' ,
			'Konten Slideshow Promo' => '' 
		);

		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    
		
	}
	/*
	* Menampilkan kontent slideshow /  divisi
    */
    public function division($division_id)
    {
    	// mengambil detail id divisi
    	$this->load->model('division');
    	$division = Division::where("id",$division_id)->first();
    	if (count($division)==0) {
    		show_404();

    		die();
    	}

    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_slides');
    	$crud->set_subject('Slide Promo');
    	$crud->where("type",2);
    	$crud->where("division_id",$division_id);

    	// column
    	$crud->columns('name','image','description','status');

    	// field
		$crud->fields('name','image','description','status');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Judul Slide");
		$crud->display_as("description","Keterangan Slide");
		$crud->display_as("image","Gambar Background");
		$crud->display_as("status","Tampilkan");

		// validation
		$crud->required_fields('name','image','description','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/slides');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		//$crud->callback_before_insert(array($this,'callback_slug'));
		//$crud->callback_before_update(array($this,'callback_slug'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		$crud->unset_texteditor('description');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Slide Promo';
		$data['crumb'] = array( 
			'Divisi '.$division->name => $this->ADMIN_NAMESPACE.'/divisions/index/edit/'.$division_id ,
			'Slide Promo' => '' 
		);

		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    
		
	}

}

/* End of file Slideshows.php */
/* Location: ./application/controllers/Interadmin/Slideshows.php */