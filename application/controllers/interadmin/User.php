<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends ADMIN_Controller {
	function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    /*
	* Menampilkan data master trip berikut CRUD nya
    */
	public function master()
	{
		$crud = new grocery_CRUD();

    	$crud->set_table('tb_users');
    	$crud->set_subject('Master Data User');
    	$crud->columns('full_name','email','phone','address','status');
    	
    	$crud->add_fields('full_name','email','phone','address','status');
		$crud->edit_fields('full_name','email','phone','address','status');

		// aliasing
		$crud->display_as("full_name","Nama User");

		// relation table

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Master Data User';
		$data['crumb'] = array( 'Master Data User' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
	}

}

/* End of file Trip.php */
/* Location: ./application/controllers/Travadmin/User.php */