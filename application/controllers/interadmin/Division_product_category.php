<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_product_category extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    public function index(){
    	// set division id
    	switch ($this->userGroup) {
    		case '5':
    			$division_id = 1;
    			break;
    		case '6':
    			$division_id = 2;
    			break;
    		case '7':
    			$division_id = 3;
    			break;
    		
    		default:
    			$division_id = 1;
    			break;
    	}
    	redirect(base_url($this->ADMIN_NAMESPACE."/division_product_category/data/".$division_id),'refresh');
    }
    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function data($division_id)
    {
    	$crud = new grocery_CRUD();

    	// Handle request filter
    	$addon_header_title = "";
  

    	$crud->set_table('tb_category_products');
    	$crud->set_subject('Kategori Produk');
    	$crud->where("division_id",$division_id);

    	// column
    	$crud->columns('name','division_id','description','parameter_specification');

    	// field
		$crud->fields('division_id','name','description','slug');

		

		// aliasing
		$crud->display_as("division_id","Divisi");
		$crud->display_as("name","Nama Kategori");

		$crud->field_type("division_id","hidden",$division_id);

		

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_column('parameter_specification',array($this,'callback_column_parameter_specification'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));
		$crud->callback_before_update(array($this,'callback_slug'));

		// script addon
		$data['script'] = '
		$(".add-anchor").html("<i class=\"fa fa-plus-circle\"></i> <span class=\"add\">Tambah Kategori Produk </span></i> ");
	
						
		';

		$data['crumb'] = array( 
			'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
			'Kategori Produk' => '' 
		);

		// State
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();

		if($state == 'list'){
			// tampilkan relasi jika state list
			$crud->set_relation('division_id', 'tb_divisions', 'name');

			$data['crumb'] = array( 
				'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
				'Kategori Produk '.$addon_header_title => '' );
			// script addon
			$division_data = $this->db->get("tb_divisions");
			$html_option_value = '';
			if ($division_data->num_rows()>0) {
				foreach ($division_data->result() as $key ) {
					$selected = "";
					if (!empty($division_id)) {
						if ($division_id == $key->id) {
							$selected = "selected";
						}
					}
					$html_option_value .= '<option value=\"'.$key->id.'\" '.$selected.'>'.$key->name.'</option>';
				}
			}
			
		}elseif($state == 'add'){
			$data['crumb'] = array( 
				'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
				'Kategori Produk' => $this->ADMIN_NAMESPACE.'/division_product_category',
				'Tambah Kategori '.$addon_header_title => '' 
			);
			if (empty($division_id)) {
				// relation
				$crud->set_relation('division_id', 'tb_divisions', 'name');
			}

			
		}elseif($state == 'edit'){
			$data['script'] = '
			
			$(".box-title").html("<i class=\"fa fa-pencil fa-fw\"></i> Edit Kategori Produk	");
							
			';

			$data['crumb'] = array( 
				'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
				'Kategori Produk' => $this->ADMIN_NAMESPACE.'/division_product_category',
				'Edit Kategori '.$addon_header_title => '' 
			);
			$crud->set_relation('division_id', 'tb_divisions', 'name');
			$crud->field_type("division_id","readonly");

			// validation
				
		}elseif($state == 'update_validation'){
			$crud->required_fields('name','description');	
		}elseif($state == "insert_validation"){
			// validation
			$crud->required_fields('division_id','name','description');
		}else{
			$data['crumb'] = array( 
				'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
				'Kategori Produk' => $this->ADMIN_NAMESPACE.'/division_product_category',
				'Detail Divisi ' => '' 
			);
			$crud->set_relation('division_id', 'tb_divisions', 'name');
			$data['script'] = '
			
			$(".box-title").html("<i class=\"fa fa-info-circle fa-fw\"></i> Detail Kategori Produk	");
							
			';
		}

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Kategori Produk';
		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    /*
	* Menampilkan parameter spesifikasi tiap kategori
    */
    public function callback_column_parameter_specification($value, $row){
    	$option = $this->db->select("*")->where("category_id",$row->id)->get("tb_category_options");
    	$result = "";
    	if ($option->num_rows()>0) {
    		$result .= "Terdapat ".$option->num_rows()." Parameter :<ol>";
    		foreach ($option->result() as $key) {
    			$result .= '<li>'.$key->name.'</li>';
    		}
    		$result .= "</ol>
    		<br>
    		<a href='".base_url($this->ADMIN_NAMESPACE."/division_option/index/".$row->id)."' class='btn btn-primary'>Klik disini untuk mengatur parameter.</a>
    		";
    	}else{
    		$result .= '
    		Tidak ditemukan parameter dalam kategori ini.
    		<br>
    		<a href="'.base_url($this->ADMIN_NAMESPACE."/division_option/index/".$row->id).'"" class="btn btn-primary">Klik disini untuk mengatur parameter.</a>

    		';
    	}
    	return $result;
    }

}

/* End of file Categories.php */
/* Location: ./application/controllers/Interadmin/Categories.php */