<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_supports extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan product supports article
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_product_supports');
    	$crud->set_subject('Product Supports');

    	// column
    	$crud->columns('name','image','status','created_at','updated_at');

    	// field
		$crud->add_fields('name','en_name','image','description','en_description','slug','status','created_at');
		$crud->edit_fields('name','en_name','image','description','en_description','slug','status','updated_at');


		// aliasing
		$crud->display_as("name","Judul Isu");
		$crud->display_as("description","Konten Isu Support");
		$crud->display_as("slug","url friendly (fill by system)");
		$crud->display_as("status","Status Publikasi");
		$crud->display_as("en_name","<i style='color:blue'>English : Judul Isu</i>");
		$crud->display_as("en_description","<i style='color:blue'>English : Konten Isu Support</i>");

		// validation
		$crud->required_fields('name','en_name','image','description','en_description','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/supports');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type('created_at','hidden', date("Y-m-d h:i:s",time()));
		$crud->field_type('updated_at','hidden', date("Y-m-d h:i:s",time()));


		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));

		// unset oepration
		$crud->unset_read();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Product Supports';
		$data['crumb'] = array( 'Product Supports' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    //toAscii
    



}

/* End of file News.php */
/* Location: ./application/controllers/Interadmin/News.php */