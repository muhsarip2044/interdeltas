<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar artikel
    */
    public function article()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_blogs');
    	$crud->set_subject('Daftar Berita & Artikel');

    	// column
    	$crud->columns('name','image','category_id','summary','slug','status');

    	// field
		$crud->fields('name','en_name','image','category_id','summary','en_summary','description','en_description','slug','status');

		// relation
		$crud->set_relation('category_id', 'tb_category_blogs', 'name');

		// aliasing
		$crud->display_as("name","Judul Artikel");
		$crud->display_as("category_id","Kategori Artikel");
		$crud->display_as("summary","Deskripsi Singkat");
		$crud->display_as("description","Konten Tulisan");
		$crud->display_as("slug","url friendly");
		$crud->display_as("en_name","<i style='color:blue'>English : Judul Artikel</i>");
		$crud->display_as("en_description","<i style='color:blue'>English : Konten Tulisan</i>");
		$crud->display_as("en_summary","<i style='color:blue'>English : Deskripsi Singkat</i>");

		// validation
		$crud->required_fields('name','image','category_id','summary','description','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/blogs');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('slug', 'readonly');

		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));
		$crud->callback_before_update(array($this,'callback_slug'));


		// unset text editor
		$crud->unset_texteditor("summary","en_summary");
		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Daftar Berita & Artikel';
		$data['crumb'] = array( 'Daftar Berita & Artikel' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    //toAscii
    



    /*
	* Menampilkan kategori artikel
    */
    public function category()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_category_blogs');
    	$crud->set_subject('Kategori Artikel');

    	// column
    	$crud->columns('name');

    	// field
		$crud->fields('name');

		// relation
		//$crud->set_relation('category_id', 'tb_category_blogs', 'name');

		// aliasing
		$crud->display_as("name","Nama Kategori");

		// validation
		$crud->required_fields('name');

   		// field upload
   		//$crud->set_field_upload('image','assets/frontend/images/blogs');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Kategori Artikel';
		$data['crumb'] = array( 'Kategori Artikel' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file News.php */
/* Location: ./application/controllers/Interadmin/News.php */