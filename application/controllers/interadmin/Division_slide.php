<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_slide extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    public function index(){
    	// set division id
    	switch ($this->userGroup) {
    		case '5':
    			$division_id = 1;
    			break;
    		case '6':
    			$division_id = 2;
    			break;
    		case '7':
    			$division_id = 3;
    			break;
    		
    		default:
    			$division_id = 1;
    			break;
    	}
    	redirect(base_url($this->ADMIN_NAMESPACE."/division_slide/division/".$division_id),'refresh');
    }

    /*
	* Menampilkan kontent slideshow /  divisi
    */
    public function division($division_id)
    {
    	// mengambil detail id divisi
    	$this->load->model('division');
    	$division = Division::where("id",$division_id)->first();
    	if (count($division)==0) {
    		show_404();

    		die();
    	}

    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_slides');
    	$crud->set_subject('Slide Promo');
    	$crud->where("type",2);
    	$crud->where("division_id",$division_id);

    	// column
    	$crud->columns('name','image','description','status');

    	// field
		$crud->fields('name','image','type','division_id','description','status');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Judul Slide");
		$crud->display_as("description","Keterangan Slide");
		$crud->display_as("image","Gambar Background");
		$crud->display_as("status","Tampilkan");

		// validation
		$crud->required_fields('name','image','description','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/slides');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type("division_id","hidden",$division_id);
		$crud->field_type("type","hidden",2);
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		//$crud->callback_before_insert(array($this,'callback_slug'));
		//$crud->callback_before_update(array($this,'callback_slug'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		$crud->unset_texteditor('description');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Slide Promo';
		$data['crumb'] = array( 
			'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard',
			'Slide Promo' => '' 
		);

		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    
		
	}
}