<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan laporan tahunan
    */
    public function annual()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_annual_reports');
    	$crud->set_subject('Annual Report');

    	// column
    	$crud->columns('name','image','file','status');

    	// field
		$crud->fields('name','image','file','status');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("name","Judul Laporan");
		$crud->display_as("image","Gambar");
		$crud->display_as("file","File Laporan (PDF)");
		$crud->display_as("status","Tampilkan");

		// validation
		$crud->required_fields('name','image','file','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/reports');
   		$crud->set_field_upload('file','assets/frontend/files/reports');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Annual Report';
		$data['crumb'] = array( 'Annual Report' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

    /*
	* Menampilkan laporan financial
    */
    public function finance()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_finance_reports');
    	$crud->set_subject('Financial Report');

    	// column
    	$crud->columns('name','quartal','sort','status');

    	// field
		$crud->fields('name','sort','status');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("name","Judul Laporan");
		$crud->display_as("status","Tampilkan");

		// validation
		$crud->required_fields('name','image','file','status');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/reports');
   		$crud->set_field_upload('file','assets/frontend/files/reports');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		 $crud->callback_column('quartal',array($this,'_callback_column_quartal'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Financial Report';
		$data['crumb'] = array( 'Financial Report' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

    public function _callback_column_quartal($value, $row)
    {
		return "<a href='".base_url($this->ADMIN_NAMESPACE.'/reports/quartal/'.$row->id)."' class='btn btn-primary'>Mengisi Laporan per Quartal</a>";
    }

    /*
	* Menampilkan laporan per quartal
	* @param integer id finance
    */
    public function quartal($finance_id=null)
    {
    	$this->load->model('finance_report');

    	$report = Finance_report::where("id",$finance_id)->first();

    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_finance_quartals');
    	$crud->where("finance_id",$finance_id);
    	$crud->set_subject('Laporan : '.$report->name);

    	// column
    	$crud->columns('name','file','sort');

    	// field
		$crud->edit_fields('name','file','sort');
		$crud->add_fields('name','finance_id','file','sort');

		// relation

		// aliasing
		$crud->display_as("name","Judul Laporan");
		$crud->display_as("sort","Urutan Tampilan");

		// validation
		$crud->required_fields('name','file','sort');

   		// field upload
   		//$crud->set_field_upload('image','assets/frontend/images/reports');
   		$crud->set_field_upload('file','assets/frontend/files/reports');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type('finance_id', 'hidden',$finance_id);

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Laporan : '.$report->name;
		$data['crumb'] = array( 
			'Financial Report' => $this->ADMIN_NAMESPACE."/reports/finance",
			'Laporan : '.$report->name => '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Reports.php */
/* Location: ./application/controllers/Interadmin/Reports.php */