<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division_catalogue extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    public function index(){
    	// set division id
    	switch ($this->userGroup) {
    		case '5':
    			$division_id = 1;
    			break;
    		case '6':
    			$division_id = 2;
    			break;
    		case '7':
    			$division_id = 3;
    			break;
    		
    		default:
    			$division_id = 1;
    			break;
    	}
    	redirect(base_url($this->ADMIN_NAMESPACE."/division_catalogue/data/".$division_id),'refresh');
    }

    /*
	* Menampilkan daftar produk
    */
    public function data($division_id=null)
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_catalogues');
    	$crud->set_subject('Katalog Produk');

    	$crud->where("division_id",$division_id);

    	// column
    	$crud->columns('name','image','description','status');

    	// field
		$crud->add_fields('name','division_id','image','file','description','status');
		$crud->edit_fields('name','image','file','description','status');

		// relation

		// aliasing
		$crud->display_as("name","Judul Katalog");
		$crud->display_as("file","File PDF");

		// validation
		$crud->required_fields('name','description');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/catalogues');
   		$crud->set_field_upload('file','assets/frontend/files/catalogues');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type("division_id","hidden",$division_id);
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Katalog Produk';
		$data['crumb'] = array(
			'Menu Utama'	=> $this->ADMIN_NAMESPACE.'/division_dashboard', 
			'Katalog Produk' => '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Catalogues.php */
/* Location: ./application/controllers/Interadmin/Catalogues.php */