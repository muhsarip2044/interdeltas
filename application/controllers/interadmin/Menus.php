<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar produk
    */
    public function index()
    {
    	$id = $this->uri->segment(5);
    	if (empty($id)) {
    		show_404();

    		die();
    	}

    	$this->load->model('menu');
    	$detail = Menu::where("id",$id)->first();

    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_menus');
    	$crud->set_subject($detail->name);

    	// column
    	$crud->columns('name','slug');

    	// Pengecualian untuk field 18
		$crud->fields('name','en_name','description','en_description','image','slug');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Judul");
		$crud->display_as("image","Gambar (Kosongkan jika tidak dibutuhkan)");
		$crud->display_as("description","Konten");
		$crud->display_as("en_name","<i style='color:blue'>English : Judul</i>");
		$crud->display_as("en_description","<i style='color:blue'>English : Konten</i>");

		// validation
		$crud->required_fields('name','description');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		$crud->field_type('slug', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = $detail->name;
		$data['crumb'] = array( $detail->name => '' );
		$data['script'] = '
		$("#save-and-go-back-button").hide();
		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Menus.php */
/* Location: ./application/controllers/Interadmin/Menus.php */