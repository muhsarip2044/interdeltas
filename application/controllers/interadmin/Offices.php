<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_branch_offices');
    	$crud->set_subject('Branch Office');

    	// column
    	$crud->columns('region','address','phone','fax');

    	// field
		$crud->fields('region','address','phone','fax');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("region","Daerah / Kota");
		$crud->display_as("address","Alamat Kantor");

		// validation
		$crud->required_fields('region','address','phone','fax');

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Branch Office';
		$data['crumb'] = array( 
			'Informasi Umum' => $this->ADMIN_NAMESPACE.'/profiles/index/edit/1',
			'Branch Office' => '' 
		);

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
}

/* End of file Offices.php */
/* Location: ./application/controllers/Interadmin/Offices.php */