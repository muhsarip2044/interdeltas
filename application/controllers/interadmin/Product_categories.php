<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_categories extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar divisi perusahaan
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	// Handle request filter
    	$addon_header_title = "";
    	if (!empty($this->input->get('division_id'))) {
    		$crud->where("division_id",$this->input->get('division_id'));
    		$crud->field_type("division_id","hidden",$this->input->get('division_id'));

    		// tambah keterangan header ketika state add/edit
    		$current_division = $this->db->where("id",$this->input->get('division_id'))->get("tb_divisions");
    		$addon_header_title .= " : Divisi ".$current_division->row()->name;
    	}

    	$crud->set_table('tb_category_products');
    	$crud->set_subject('Kategori Produk');

    	// column
    	$crud->columns('name','division_id','description','parameter_specification');

    	// field
		$crud->fields('division_id','name','description','slug');

		

		// aliasing
		$crud->display_as("division_id","Divisi");
		$crud->display_as("name","Nama Kategori");

		

   		// field upload
   		//$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_column('parameter_specification',array($this,'callback_column_parameter_specification'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));
		//CALLBACKS
		$crud->callback_before_insert(array($this,'callback_slug'));
		$crud->callback_before_update(array($this,'callback_slug'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		// script addon
		$data['script'] = '
		$(".add-anchor").html("<i class=\"fa fa-plus-circle\"></i> <span class=\"add\">Tambah Kategori Produk </span></i> ");
		$(".box-title").html("<i class=\"fa fa-plus-circle fa-fw\"></i> Tambah Kategori Produk	");
						
		';

		$data['crumb'] = array( 'Kategori Produk' => '' );

		// State
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();

		if($state == 'list'){
			// tampilkan relasi jika state list
			$crud->set_relation('division_id', 'tb_divisions', 'name');

			$data['crumb'] = array( 'Kategori Produk '.$addon_header_title => '' );
			// script addon
			$division_data = $this->db->get("tb_divisions");
			$html_option_value = '';
			if ($division_data->num_rows()>0) {
				foreach ($division_data->result() as $key ) {
					$selected = "";
					if (!empty($this->input->get('division_id'))) {
						if ($this->input->get('division_id') == $key->id) {
							$selected = "selected";
						}
					}
					$html_option_value .= '<option value=\"'.$key->id.'\" '.$selected.'>'.$key->name.'</option>';
				}
			}
			
			$data['script'] = '
			// if filter aktif
			var division_id = "'.$this->input->get('division_id').'";
			if (division_id != "") {
				$(".add-anchor").attr("href","'.base_url($this->ADMIN_NAMESPACE."/product_categories/index/add?division_id=").'" + division_id);
			}

			$(".add-anchor").html("<i class=\"fa fa-plus-circle\"></i> <span class=\"add\">Tambah Kategori Produk '.$addon_header_title.'</span></i> ");
			$(".box-header").append("<br><br><label><i class=\"fa fa-filter\"></i> Filter Divisi</label><select id=\"selectFilter\" class=\"form-control text-center \"><option value=\"all\">Semua Divisi</option>'.$html_option_value.'</select>")
					
			// event flter division id
			$("#selectFilter").on("change",function(){
				var val = $(this).val();
				var base_url = "'.base_url($this->ADMIN_NAMESPACE).'/";
				if (val == "all") {
					window.location.href = base_url + "product_categories";
				}else{
					window.location.href = base_url + "product_categories?division_id=" + val;
				}
			});

			';
		}elseif($state == 'add'){
			$data['crumb'] = array( 
				'Kategori Produk' => $this->ADMIN_NAMESPACE.'/product_categories',
				'Tambah Kategori '.$addon_header_title => '' 
			);
			if (empty($this->input->get('division_id'))) {
				// relation
				$crud->set_relation('division_id', 'tb_divisions', 'name');
			}

			
		}elseif($state == 'edit'){
			$data['script'] = '
			
			$(".box-title").html("<i class=\"fa fa-pencil fa-fw\"></i> Edit Kategori Produk	");
							
			';

			$data['crumb'] = array( 
				'Kategori Produk' => $this->ADMIN_NAMESPACE.'/product_categories',
				'Edit Kategori '.$addon_header_title => '' 
			);
			$crud->set_relation('division_id', 'tb_divisions', 'name');
			$crud->field_type("division_id","readonly");

			// validation
				
		}elseif($state == 'update_validation'){
			$crud->required_fields('name','description');	
		}elseif($state == "insert_validation"){
			// validation
			$crud->required_fields('division_id','name','description');
		}else{
			$data['crumb'] = array( 
				'Kategori Produk' => $this->ADMIN_NAMESPACE.'/product_categories',
				'Detail Divisi ' => '' 
			);
			$crud->set_relation('division_id', 'tb_divisions', 'name');
			$data['script'] = '
			
			$(".box-title").html("<i class=\"fa fa-info-circle fa-fw\"></i> Detail Kategori Produk	");
							
			';
		}

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Kategori Produk';
		

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    /*
	* Menampilkan parameter spesifikasi tiap kategori
    */
    public function callback_column_parameter_specification($value, $row){
    	$option = $this->db->select("*")->where("category_id",$row->id)->get("tb_category_options");
    	$result = "";
    	if ($option->num_rows()>0) {
    		$result .= "Terdapat ".$option->num_rows()." Parameter :<ol>";
    		foreach ($option->result() as $key) {
    			$result .= '<li>'.$key->name.'</li>';
    		}
    		$result .= "</ol>
    		<br>
    		<a href='".base_url($this->ADMIN_NAMESPACE."/options?category_id=".$row->id)."' class='btn btn-primary'>Klik disini untuk mengatur parameter.</a>
    		";
    	}else{
    		$result .= '
    		Tidak ditemukan parameter dalam kategori ini.
    		<br>
    		<a href="'.base_url($this->ADMIN_NAMESPACE."/options?category_id=".$row->id).'"" class="btn btn-primary">Klik disini untuk mengatur parameter.</a>

    		';
    	}
    	return $result;
    }

}

/* End of file Categories.php */
/* Location: ./application/controllers/Interadmin/Categories.php */