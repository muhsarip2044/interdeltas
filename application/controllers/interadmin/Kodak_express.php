<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kodak_express extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar outlet kodak express
    */
    public function outlet()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_kodak_express_outlets');
    	$crud->set_subject('Kodak Express Outlet');

    	// column
    	$crud->columns('outlet_name','region_id','address');

    	// field
		$crud->fields('outlet_name','region_id','address');

		// relation
		$crud->set_relation('region_id', 'tb_regions', 'name');

		// aliasing
		$crud->display_as("region_id","Daerah");
		$crud->display_as("outlet_name","Nama Outlet");

		// validation
		$crud->required_fields('outlet_name','region_id','address');

   		// field upload
   		//$crud->set_field_upload('image','assets/frontend/images/reports');
   		//$crud->set_field_upload('file','assets/frontend/files/reports');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Kodak Express Outlet';
		$data['crumb'] = array( 'Kodak Express Outlet' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

    /*
	* Menampilkan daftar region kodak express
    */
    public function region()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_regions');
    	$crud->set_subject('Daerah Cakupan Kodak Express');

    	// column
    	$crud->columns('name');

    	// field
		$crud->fields('name');

		// relation
		//$crud->set_relation('region_id', 'tb_regions', 'name');

		// aliasing
		$crud->display_as('name',"Nama Daerah");

		// validation
		//$crud->required_fields('outlet_name','region_id','address');

   		// field upload
   		//$crud->set_field_upload('image','assets/frontend/images/reports');
   		//$crud->set_field_upload('file','assets/frontend/files/reports');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Daerah Cakupan Kodak Express';
		$data['crumb'] = array( 'Daerah Cakupan Kodak Express' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Kodak_express.php */
/* Location: ./application/controllers/Interadmin/Kodak_express.php */