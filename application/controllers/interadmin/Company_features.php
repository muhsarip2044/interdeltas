<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_features extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan kontent slideshow
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_company_features');
    	$crud->set_subject('Point-point Layanan Perusahaan');

    	// column
    	$crud->columns('name','icon','description');

    	// field
		$crud->fields('name','icon','description');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');
		//$crud->set_relation('category_id', 'tb_category_products', 'name');

		// aliasing
		$crud->display_as("name","Point");
		$crud->display_as("description","Keterangan");
		$crud->display_as("status","Tampilkan");

		// validation
		$crud->required_fields('name','icon','description');

   		// field upload
   		//$crud->set_field_upload('image','assets/frontend/images/slides');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		//$crud->callback_before_insert(array($this,'callback_slug'));
		//$crud->callback_before_update(array($this,'callback_slug'));
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		$crud->callback_field('icon',array($this,'callback_field_icon'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		$crud->unset_texteditor('description');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Point-point Layanan Perusahaan';
		$data['crumb'] = array( 
			'Konten Homepage' => 'interadmin/homepages/index/edit/1' ,
			'Point-point Layanan Perusahaan' => '' 
		);

		$data['script'] = '
		$(document).ready(function(){
		    $(".icon-input").removeClass("form-control");
		});
			


		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    
		
	}
	function callback_field_icon($value = '', $primary_key = null)
	{
		$this->load->model('icon');
		$icon = Icon::all();
		$option = '<div class="row" style="max-height:300px;overflow:scroll">';
		if (count($icon)>0) {
			foreach ($icon as $key ) {
				$checked = "";
				if ($key->name == $value) {
					$checked="checked";
				}
				$option .= '
				<div class="col-xs-3 col-md-2 member-icon" style="height:50px;">
					<div class="radio">
	                    <label>
	                      <input type="radio" name="icon" value="'.$key->name.'" class="icon-input"  '.$checked.'>
	                      <i class="fa fa-'.$key->name.'"></i> '.$key->name.'
	                    </label>
	                  </div>
					
					
				</div>
				';
			}
		}
		$option .='</div>';
		return $option;
	}
}

/* End of file Company_features.php */
/* Location: ./application/controllers/Interadmin/Company_features.php */