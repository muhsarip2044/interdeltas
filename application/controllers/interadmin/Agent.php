<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends ADMIN_Controller {
	function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
    /*
	* Menampilkan data master trip berikut CRUD nya
    */
	public function master()
	{
		$crud = new grocery_CRUD();

    	$crud->set_table('tb_agents');
    	$crud->set_subject('Master Data Agent');
    	$crud->columns('name','company','address','phone','image','status');
    	
    	$crud->add_fields('name','company','address','phone','image','status');
		$crud->edit_fields('name','company','address','phone','image','status');

		// aliasing
		$crud->display_as("company","Company (Nama PT)");

		// relation table

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Master Data Agent';
		$data['crumb'] = array( 'Master Data Agent' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
	}

}

/* End of file Trip.php */
/* Location: ./application/controllers/Travadmin/Agent.php */