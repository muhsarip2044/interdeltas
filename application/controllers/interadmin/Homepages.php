<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepages extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan konten edit homepage
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_homepages');
    	$crud->set_subject('Konten Homepage');

    	// column
    	$crud->columns('slideshow','segment_1_title','segment_1_description','company_feature','segment_2_description');

    	// field
		$crud->fields('slideshow','segment_1_title','en_segment_1_title','segment_1_description','en_segment_1_description','segment_2_description','en_segment_2_description','company_feature',"featuring_product");

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("segment_1_title","Judul segmen pembuka");
		$crud->display_as("segment_1_description","Konten segmen pembuka");
		$crud->display_as("segment_2_description","Tulisan Highlight");
		$crud->display_as("en_segment_1_title","<i style='color:blue'>English : Judul segmen pembuka</i>");
		$crud->display_as("en_segment_1_description","<i style='color:blue'>English : Konten segmen pembuka</i>");
		$crud->display_as("en_segment_2_description","<i style='color:blue'>English : Tulisan Highlight</i>");

		$crud->display_as("company_feature","Point-point Layanan Perusahaan");
		$crud->display_as("slideshow","Slideshow Promo");


		// validation
		$crud->required_fields('segment_1_title','segment_1_description','segment_2_description');

   		// field upload

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		$crud->callback_field('slideshow',array($this,'callback_column_slideshow'));
		$crud->callback_field('company_feature',array($this,'callback_column_feature'));
		$crud->callback_field('featuring_product',array($this,'callback_field_featuring_product'));
		//$crud->callback_column('slideshow',array($this,'callback_column_slideshow'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();
		$crud->unset_texteditor('segment_1_description','segment_2_description','en_segment_1_description','en_segment_2_description');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Konten Homepage';
		$data['crumb'] = array( 'Konten Homepage' => '' );

		$data['script'] = '
			$("#save-and-go-back-button").hide();
		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    public function callback_column_slideshow($value = '', $primary_key = null)
	{
		return '<a href="'.base_url($this->ADMIN_NAMESPACE.'/slideshows').'" class="btn btn-primary btn-lg btn-block"><i class="fa fa-chevron-right"></i> Edit Slideshow</a>';
	}
	public function callback_column_feature($value = '', $primary_key = null)
	{
		return '<a href="'.base_url($this->ADMIN_NAMESPACE.'/company_features').'" class="btn btn-primary btn-lg btn-block"><i class="fa fa-chevron-right"></i> Kelola Point-point Layanan</a>';
	}
	public function callback_field_featuring_product($value = '', $primary_key = null)
	{
		return '<a href="'.base_url($this->ADMIN_NAMESPACE.'/Product_features').'" class="btn btn-primary btn-lg btn-block"><i class="fa fa-chevron-right"></i> Kelola Featuring Product</a>';
	}

}

/* End of file Homepages.php */
/* Location: ./application/controllers/Interadmin/Homepages.php */