<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profiles extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan konten edit homepage
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_components');
    	$crud->set_subject('Informasi Umum');

    	// column
 

    	// field
		$crud->fields('title','company','phone','email','address','branch','office_hour','logo','logo_white','favicon','footer');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("title","Brand Persuhaan");
		$crud->display_as("company","Nama Perusahaan");
		$crud->display_as("address","Head Office");
		$crud->display_as("branch","Branch Office");
		$crud->display_as("office_hour","Jam Kerja");
		$crud->display_as("logo","Logo Perusahaan");
		$crud->display_as("logo_white","Logo (Warna Putih)");
		$crud->display_as("favicon","Icon");
		$crud->display_as("footer","Lisensi Website");


		// validation
		$crud->required_fields('title','company','phone','email','address','office_hour','logo','logo_white','favicon','footer');

   		// field upload
   		$crud->set_field_upload('logo','assets/frontend/images');
   		$crud->set_field_upload('logo_white','assets/frontend/images');
   		$crud->set_field_upload('favicon','assets/frontend/images');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		$crud->callback_field('branch',array($this,'callback_column_branch'));
		//$crud->callback_column('slideshow',array($this,'callback_column_slideshow'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();
		$crud->unset_texteditor('address','footer');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Informasi Umum';
		$data['crumb'] = array( 'Informasi Umum' => '' );

		$data['script'] = '
			$("#save-and-go-back-button").hide();
		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }
    public function callback_column_branch($value = '', $primary_key = null)
	{
		return '<a href="'.base_url($this->ADMIN_NAMESPACE.'/offices').'" target="_blank" class="btn btn-primary btn-lg btn-block"><i class="fa fa-chevron-right"></i> Kelola Branch Office</a>';
	}

	/*
	* Menampilkan konten SEO Website
    */
    public function seo()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_components');
    	$crud->set_subject('SEO Website');

    	// column
 

    	// field
		$crud->fields('seo_author','seo_description','seo_keyword');

		// relation
		//$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("seo_author","Pemilik Website");
		$crud->display_as("seo_description","Deskripsi website");
		$crud->display_as("seo_keyword","Kata Kunci Website");


		// validation
		$crud->required_fields('seo_author','seo_description','seo_keyword');

   		// field upload
   		// $crud->set_field_upload('logo','assets/frontend/images');
   		// $crud->set_field_upload('logo_white','assets/frontend/images');
   		// $crud->set_field_upload('favicon','assets/frontend/images');

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		//$crud->callback_field('branch',array($this,'callback_column_branch'));
		//$crud->callback_column('slideshow',array($this,'callback_column_slideshow'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();
		//$crud->unset_texteditor('address','footer');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'SEO Website';
		$data['crumb'] = array( 'SEO Website' => '' );

		$data['script'] = '
			$("#save-and-go-back-button").hide();
		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Profiles.php */
/* Location: ./application/controllers/Interadmin/Profiles.php */