<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisions extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* 
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('users');
    	$crud->set_subject('Users');

    	// column
    	$crud->columns('username','email','image','groups','active');

    	// field
    	$crud->add_fields('id');
		$crud->edit_fields('');

		// relation
		$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');

		// validation
		$crud->required_fields('name');

   		// field upload
   		$crud->set_field_upload('image','assets/backend/images');

		//FIELD TYPES
		$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		$crud->callback_insert(array($this, 'create_user_callback'));
		$crud->callback_update(array($this, 'edit_user_callback'));
		$crud->callback_field('last_login',array($this,'last_login_callback'));
		$crud->callback_column('active',array($this,'active_callback'));

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Users';
		$data['crumb'] = array( 'Users' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Divisions.php */
/* Location: ./application/controllers/Interadmin/Divisions.php */