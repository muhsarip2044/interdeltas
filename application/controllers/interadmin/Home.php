<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends ADMIN_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }
 	
    public function index()
    {
    	
		$output = (object)array('data' => '','output' => '' , 'js_files' => null , 'css_files' => null);
		
		$data['judul'] = 'Dashboard';

		$template = 'master';
		$view = 'backend/dashboard';
		$this->outputview->output_admin($view, $template, $data, $output);
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Admin/Home.php */