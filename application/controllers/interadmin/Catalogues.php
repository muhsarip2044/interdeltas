<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogues extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan daftar produk
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_catalogues');
    	$crud->set_subject('Katalog Produk');

    	// column
    	$crud->columns('name','image','division_id','description','status');

    	// field
		$crud->fields('name','division_id','image','file','description','status');

		// relation
		$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("division_id","Divisi");
		$crud->display_as("name","Judul Katalog");
		$crud->display_as("file","File PDF");

		// validation
		$crud->required_fields('name','division_id','description');

   		// field upload
   		$crud->set_field_upload('image','assets/frontend/images/catalogues');
   		$crud->set_field_upload('file','assets/frontend/files/catalogues');

		//FIELD TYPES
		$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		// $crud->callback_field('last_login',array($this,'last_login_callback'));
		// $crud->callback_column('active',array($this,'active_callback'));

		// unset oepration
		//$crud->unset_add();
		//$crud->unset_delete();

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Katalog Produk';
		$data['crumb'] = array( 'Katalog Produk' => '' );

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Catalogues.php */
/* Location: ./application/controllers/Interadmin/Catalogues.php */