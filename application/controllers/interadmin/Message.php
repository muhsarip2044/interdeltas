<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends ADMIN_Controller {
 
    function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('OutputView');
    }

    /*
	* Menampilkan konten edit homepage
    */
    public function index()
    {
    	$crud = new grocery_CRUD();

    	$crud->set_table('tb_messages');
    	$crud->set_subject('Kotak Pesan Visitor');

    	// column
    	$crud->columns('division_id','name','email','subject','message','created_at');

    	// field
		//$crud->fields('slideshow','segment_1_title','segment_1_description','company_feature','segment_2_description');

		// relation
		$crud->set_relation('division_id', 'tb_divisions', 'name');

		// aliasing
		$crud->display_as("division_id","Ditujukan Untuk Divisi");
		$crud->display_as("name","Nama Pengirim");
		$crud->display_as("email","Email Pengirim");
		$crud->display_as("subject","Judul Pesan");
		$crud->display_as("message","Isi Pesan");
		$crud->display_as("created_at","Dikirim Pada");


		// validation
		//$crud->required_fields('segment_1_title','segment_1_description','segment_2_description');

   		// field upload

		//FIELD TYPES
		//$crud->field_type("status","dropdown",array(1=>'Tampilkan', 0 => 'Tidak Tampilkan'));
		//$crud->field_type('last_login', 'readonly');

		//CALLBACKS
		// $crud->callback_insert(array($this, 'create_user_callback'));
		// $crud->callback_update(array($this, 'edit_user_callback'));
		//$crud->callback_field('slideshow',array($this,'callback_column_slideshow'));
		//$crud->callback_field('company_feature',array($this,'callback_column_feature'));
		//$crud->callback_column('slideshow',array($this,'callback_column_slideshow'));

		// unset oepration
		$crud->unset_add();
		$crud->unset_edit();
		//$crud->unset_texteditor('segment_1_description','segment_2_description');

		//VIEW
		$output = $crud->render();
		$data['judul'] = 'Kotak Pesan Visitor';
		$data['crumb'] = array( 'Kotak Pesan Visitor' => '' );

		$data['script'] = '
			$("#save-and-go-back-button").hide();
		';

		$template = 'master';
		$view = 'backend/grocery';
		$this->outputview->output_admin($view, $template, $data, $output);
    }

}

/* End of file Message.php */
/* Location: ./application/controllers/Interadmin/Message.php */