<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
		/*
		* Penampil sub
		*/
	public function index($tag_id=null)
	{
		// mencari keyword pada product
		$this->load->model('product');
		$this->load->model('product_tag');
		$this->load->model('tag');
		$this->load->model('category_product');


		// cek valid tag id
		$tag = Tag::where("id",$tag_id)->first();
		if (count($tag)==0) {
			// id product tidak valid, tampilkan halaman 404

			show_404();
			die();
		}


		$data['products'] = Product::where("tb_product_tags.tag_id",$tag_id)
		->join('tb_product_tags','tb_product_tags.product_id','=','tb_products.id')
	    ->get();


		$data['menu_homepage'] = $this->getMenuHomepage();
    
		
		$data['title'] 	= "Tag ".$tag->name;
		$data['tag'] = $tag->name;

		// init tag
		$data['tags'] = Tag::all();


		$data['category'] = Division::with("category")->get();
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/tag', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	

	}

}

/* End of file Tags.php */
/* Location: ./application/controllers/Tags.php */