<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CLIENT_Controller {
 
	public function __construct()
	{
		parent::__construct();
	}
	/*
	* Penampil halaman utama
	*/
	public function index() 
	{
        
        
        $data['menu_homepage'] = $this->getMenuHomepage();

        // konten khusus homeapge
        $this->load->model('homepage');
        $this->load->model('company_feature');
        $data['homepage'] = Homepage::
            select(
                $this->global_lang."segment_1_title".($this->global_lang!=''?' as segment_1_title':''),
                $this->global_lang."segment_1_description".($this->global_lang!=''?' as segment_1_description':''),
                $this->global_lang."segment_2_description".($this->global_lang!=''?' as segment_2_description':'')
            )
            ->where("id",1)->first();
        $data['company_feature'] = Company_feature::
            select(
                $this->global_lang."name".($this->global_lang!=''?' as name':''),
                $this->global_lang."description".($this->global_lang!=''?' as description':''),
                'icon'
            )
            ->get();


        // init slide show
        $this->load->model('slide');
        $this->load->model('division');
        $data['slide'] = Slide::
            select(
                $this->global_lang."name".($this->global_lang!=''?' as name':''),
                $this->global_lang."description".($this->global_lang!=''?' as description':''),
                'image'
            )
            ->where("status",1)
            ->where("type",1)->with("division")->get();

        // menampilkan blog, batas 3 artikel
        $this->load->model('blog');
        $this->load->model('blog_category');
        $data['blog'] = Blog::where("status",1)
            ->select(
                "id",
                "category_id",
                $this->global_lang."name".($this->global_lang!=''?' as name':''),
                "slug",
                $this->global_lang."summary".($this->global_lang!=''?' as summary':''),
                "created_at",
                "image"
            )
            ->with("category")
            ->limit(3)->get();

        // menampilkan 3 produk terakhir masing2 divisi
        $this->load->model('product');
        $this->load->model('category_product');

        $products = array();

        $div_prod = Division::all();

        if (count($div_prod)>0) {
            $i = 0;
            foreach ($div_prod as $key) {
                $products[$i] = Product::where("division_id",$key->id)->limit(3)->orderBy("created_at","desc")->get();
                $i++;
            }
        }
        $data['product_division'] = $products;



		$data['title'] 	= "Inter Delta";
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/homepage', $data, true);
		$this->load->view($this->folder_view_frontend.'master_homepage',$data);	

	}
    public function iterates(){
        die();
        $image = array('product-1.jpg','product-2.jpg','product-3.jpg','product-4.jpg');
        

        $this->load->model('product');

        for($i=1;$i<21;$i++){
            // rand image
            $number_random = array_rand($image);
            $get_image = $image[$number_random];

            // rand division
            $division_id = rand(1,3);

            $prod = new Product;
            $prod->name = "Sample Product ".$i;
            $prod->slug = "sample-product-".$i;
            $prod->division_id = $division_id;
            $prod->image = $get_image;
            $prod->category_id = $division_id;
            $prod->description = "<p>lorem ipsum dolor sit amet</p>";

            $prod->save();
            echo "sukses<br>";

        }
    }

    /*
    * Mengganti bahasa
    */
    public function change_language($lang=""){
        $array = array(
            'lang' => $lang
        );
        
        $this->session->set_userdata( $array );
        
        //echo $this->session->userdata('lang'); die();

        redirect(base_url(),'refresh');
    }
}
