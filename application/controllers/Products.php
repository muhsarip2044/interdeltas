<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Products extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* Penampil sub
	*/
	public function detail($slug_division=null,$slug_product=null)
	{
		// Menghasilkan template sesuai divisi
		$data_division = $this->get_header_division($slug_division);
		$data['header_navbar'] = $data_division['html'];

		$data['division'] = $data_division['data']; 

		// Mencari ID menu
		$this->load->model('product');
		$this->load->model('tag');
		$this->load->model('product_tag');
		$this->load->model('brand');
		
		$product = Product::where("slug",$slug_product)->with(["division","tags","brand"])
		->first();

		if (count($product)==0) {
			// id product tidak valid, tampilkan halaman 404

			show_404();
			die();
		}	
		$data['product'] = $product;

		// specification
		$data['specification'] = $this->db
			->select("
				tb_category_option_specs.name as 'value',
				tb_category_options.name as 'option'
			")
			->join("tb_category_option_specs","tb_product_specs.spec_id=tb_category_option_specs.id")
			->join("tb_category_options","tb_category_options.id=tb_category_option_specs.option_id")
			->where("tb_product_specs.product_id",$product->id)
			->get("tb_product_specs");
		$data['last_query'] = $this->db->last_query();


		// init tag
		$data['tags'] = Tag::all();

		$data['title'] 	= $product->name;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/product', $data, true);
		$this->load->view($this->folder_view_frontend.'master_division',$data);	

	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */