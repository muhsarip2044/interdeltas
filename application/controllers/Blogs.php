<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct(); 
	}
		/*
		* Penampil sub
		*/
	public function index()
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// get last article, batas 3
    	$this->load->model('blog');
    	$this->load->model('blog_category');

		// Menampilkan product2 / divisi
	    $per_page = 9;
	    if (!empty($_GET['page'])) {
	    	$offset = $_GET['page'];
	    }else{
	    	$offset = 0;
	    }
	    $blog = Blog::select(
	    		"id",
	    		"category_id",
	    		$this->global_lang."name".($this->global_lang!=''?' as name':''),
	    		"slug",
	    		$this->global_lang."summary".($this->global_lang!=''?' as summary':''),
	    		"created_at",
	    		"image"
	    	)
			->orderBy("created_at","desc")
	    	->limit($per_page)
	    	->offset($offset)
	    	->get();

	    // Menampilkan paginasi
	    $this->load->library('pagination_helper');
	    $total_data = Blog::count();
	    $data['total_data'] = $total_data;
	    $data['per_page'] = $per_page;
	    $this->pagination_helper->config($total_data,$per_page,'blogs');
        $data['render_pagination'] = $this->pagination_helper->render();

		$data['title'] 	= "News Dan Artikel";
		$data['category'] = Blog_category::all();
		$data['blog'] = $blog;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/blog', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	

	}
	public function read($slug=null)
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// get last article, batas 3
    	$this->load->model('blog');
    	$this->load->model('blog_category');
		$detail = Blog::
			select(
				$this->global_lang."name".($this->global_lang!=''?' as name':''),
				$this->global_lang."description".($this->global_lang!=''?' as description':''),
				"created_at",
				"image"
			)
			->where("slug",$slug)
			->first();

		if (count($detail)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}

		$data['title'] 	= $detail->name;
		$data['category'] = Blog_category::all();
		$data['detail'] = $detail;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/blog_detail', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	
	}
	public function category($category_id)
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// get last article, batas 3
    	$this->load->model('blog');
    	$this->load->model('blog_category');
		$category = Blog_category::where("id",$category_id)->with("blog")
			->first();

		if (count($category)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}

		$data['title'] 	= $category->name;
		$data['category'] = Blog_category::all();
		$data['blog'] = $category->blog;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/blog_category', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	

	}

}

/* End of file Blogs.php */
/* Location: ./application/controllers/Blogs.php */