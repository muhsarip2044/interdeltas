<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisions extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	} 
	/*
	* Penampil sub
	*/
	public function index($slug=null)
	{
		// Menghasilkan template sesuai divisi
		$data_division = $this->get_header_division($slug);
		$data['header_navbar'] = $data_division['html'];

		$data['division'] = $data_division['data'];

		// init slide show
	    $this->load->model('slide');
	    $this->load->model('division');
	    $data['slide'] = Slide::where("status",1)->where("type",2)->where("division_id",$data_division['data']->id)->with("division")->get();

	    

	    // Menampilkan product2 / divisi
	    $per_page = 9;
	    if (!empty($_GET['page'])) {
	    	$offset = $_GET['page'];
	    }else{
	    	$offset = 0;
	    }
	    $data['products'] = Product::where("division_id",$data_division['data']->id)
	    	->orderBy("created_at","desc")
	    	->limit($per_page)
	    	->offset($offset)
	    	->get();

	    // Menampilkan paginasi
	    $this->load->library('pagination_helper');
	    $total_data = Product::where("division_id",$data_division['data']->id)
	    	->orderBy("created_at","desc")->count();
	    $this->pagination_helper->config($total_data,$per_page,'divisions/'.$slug);
        $data['render_pagination'] = $this->pagination_helper->render();

		// init tag
		$this->load->model('tag');
		$data['tags'] = Tag::all();

		$data['title'] 	= $data_division['data']->name;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/division', $data, true);
		$this->load->view($this->folder_view_frontend.'master_division',$data);	

	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */