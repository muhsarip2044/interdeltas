<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* Penampil sub
	*/
	public function index($slug=null)
	{
		// Menghasilkan template sesuai divisi
		$data_division = $this->get_header_division($slug);
		$data['header_navbar'] = $data_division['html'];
		$data['division'] = $data_division['data'];

		// load valdiaiton
		$data['submit_message_success'] = false;
		$this->load->library('form_validation');

		if (!empty($this->input->post('division_id'))) {
			$config = array(
				array(
		                'field' => 'division_id',
		                'label' => 'Divisi ID',
		                'rules' => 'required|max_length[3]',
		                'errors' => array(
		                        'required' => 'Pesan tidak valid.',
		                        'max_length' => 'Pesan tidak valid.',
		                ),
		        ),
		        array(
		                'field' => 'name',
		                'label' => 'Nama',
		                'rules' => 'required|max_length[50]',
		                'errors' => array(
		                        'required' => 'Kolom %s harus di isi.',
		                        'max_length' => 'Maksimal karakter %s yang bisa diisi hanya 50 karakter.',
		                ),
		        ),
		        array(
		                'field' => 'email',
		                'label' => 'Email',
		                'rules' => 'required|max_length[50]',
		                'errors' => array(
		                        'required' => 'Kolom %s harus di isi.',
		                        'max_length' => 'Maksimal karakter %s yang bisa diisi hanya 50 karakter.',
		                ),
		        ),
		        array(
		                'field' => 'subject',
		                'label' => 'Subject',
		                'rules' => 'required|max_length[200]',
		                'errors' => array(
		                        'required' => 'Kolom %s harus di isi.',
		                        'max_length' => 'Maksimal karakter %s yang bisa diisi hanya 200 karakter.',
		                ),
		        ),
		        array(
		                'field' => 'message',
		                'label' => 'Message',
		                'rules' => 'required|max_length[2000]',
		                'errors' => array(
		                        'required' => 'Kolom %s harus di isi.',
		                        'max_length' => 'Maksimal karakter %s yang bisa diisi hanya 2000 karakter.',
		                ),
		        ),
			);

			$this->form_validation->set_rules($config);
				// Validasi berhasil, insert daya ke table
				$this->load->model('message');

				// sanitize input
				$division_id = htmlspecialchars($this->input->post('division_id',true));
				$name = htmlspecialchars($this->input->post('name',true));
				$email = htmlspecialchars($this->input->post('email',true));
				$subject = htmlspecialchars($this->input->post('subject',true));
				$messages = htmlspecialchars($this->input->post('message',true));

				$message = new Message;
				$message->division_id = $division_id;
				$message->name = $name;
				$message->email = $email;
				$message->subject = $subject;
				$message->message = $messages;

				$message->save();

				// forward email ke divisi
				$this->load->library('notification_helper');
				$this->notification_helper->forward_message($email,$name,$subject,$messages,$division_id);


	        if ($this->form_validation->run() == FALSE){
	        	
	        }else{
	        	$data['submit_message_success'] = true;
	        }
		}
		


		// init slide show

		$data['title'] 	= "Kontak Divisi ".$data_division['data']->name;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/contact', $data, true);
		$this->load->view($this->folder_view_frontend.'master_division',$data);	

	}

}

/* End of file Contact.php */
/* Location: ./application/controllers/Contact.php */