<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
		/*
		* Penampil sub
		*/
	public function index($slug=null){
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// load model menu
		$this->load->model('menu');
		$detail = Menu::where("slug",$slug)->first();

		if (count($detail)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}
		$lang_name = $this->global_lang.'name';
		$data['title'] 	= $detail->$lang_name;
		$data['detail'] = $detail;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/page', $data, true);
		$this->load->view($this->folder_view_frontend.'master_page',$data);	

	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */