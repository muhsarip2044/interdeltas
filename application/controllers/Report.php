<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
		/*
		* Penampil sub
		*/
	public function financial()
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// load model menu
		$this->load->model('finance_report');
		$this->load->model('finance_quartal');
		$report = Finance_report::with("quartal")->orderBy("sort","asc")->get();

		if (count($report)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}

		$data['title'] 	= "Financial Report ".$this->component->company;
		$data['report'] = $report;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/finance', $data, true);
		$this->load->view($this->folder_view_frontend.'master_page',$data);	

	}

	public function annual()
	{
			// init menu
	    $data['menu_homepage'] = $this->getMenuHomepage();
    
		// load model menu
		$this->load->model('annual_report');
		$report = Annual_report::orderBy("sort","asc")->get();

		if (count($report)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}

		$data['title'] 	= "Annual Report ".$this->component->company;
		$data['report'] = $report;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/annual', $data, true);
		$this->load->view($this->folder_view_frontend.'master_page',$data);	

	}

}

/* End of file Report.php */
/* Location: ./application/controllers/Report.php */