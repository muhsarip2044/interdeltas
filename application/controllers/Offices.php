<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
		/*
		* Penampil sub
		*/
	public function index()
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// load model menu
		$this->load->model('branch_office');
		$office = Branch_office::all();

		if (count($office)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}

		$data['title'] 	= "Head Office dan Branch Office";
		$data['office'] = $office;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/office', $data, true);
		$this->load->view($this->folder_view_frontend.'master_page',$data);	

	}
	public function kodak_express()
	{
		$data['menu_homepage'] = $this->getMenuHomepage();

	    // load data kodak express
	    $detail = Menu::where("id",9)->first();
	    if (count($detail)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}
    
		// load model region dan outet
		$this->load->model('region');
		$this->load->model('kodak_express');
		$region = Region::with("outlet")->get();

		if (count($region)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}


		$data['title'] 	= "Head Office dan Branch Office";
		$data['region'] = $region;
		$data['detail'] = $detail;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/kodak_express', $data, true);
		$this->load->view($this->folder_view_frontend.'master_page',$data);	
	}

}

/* End of file Offices.php */
/* Location: ./application/controllers/Offices.php */