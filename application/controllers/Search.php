<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
		/*
		* Penampil sub
		*/
	public function index()
	{
		// cek validation keyword
		$keyword = htmlspecialchars(strip_tags($this->input->post('keyword')));
		if (empty($keyword)) {
			// tidak ada keyword yang dikirim
			show_404();

			die();
		}

		// mencari keyword pada product
		$this->load->model('product');
		$this->load->model('category_product');
		$data['products'] = Product::where("name","like","%".$keyword."%")->get();


		$data['menu_homepage'] = $this->getMenuHomepage();
    
		
		$data['title'] 	= "Hasil pencarian : ".$keyword;
		$data['keyword'] = $keyword;


		$data['category'] = Division::with("category")->get();
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/search', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	

	}

}

/* End of file Search.php */
/* Location: ./application/controllers/Search.php */