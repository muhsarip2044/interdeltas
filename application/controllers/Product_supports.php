<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_supports extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct(); 
	}
		/*
		* Penampil sub
		*/
	public function index()
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// get last article, batas 3
    	$this->load->model('product_support');

		// Menampilkan product2 / divisi
	    $per_page = 9;
	    if (!empty($_GET['page'])) {
	    	$offset = $_GET['page'];
	    }else{
	    	$offset = 0;
	    }
	    $product_support = Product_support::select(
	    		"id",
	    		$this->global_lang."name".($this->global_lang!=''?' as name':''),
	    		"slug",
	    		"created_at",
	    		"image"
	    	)
			->orderBy("created_at","desc")
	    	->limit($per_page)
	    	->offset($offset)
	    	->get();

	    // Menampilkan paginasi
	    $this->load->library('pagination_helper');
	    $total_data = Product_support::count();
	    $data['total_data'] = $total_data;
	    $data['per_page'] = $per_page;
	    $this->pagination_helper->config($total_data,$per_page,'blogs');
        $data['render_pagination'] = $this->pagination_helper->render();

		$data['title'] 	= "Product Supports";
		$data['product_support'] = $product_support;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/product_support', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	

	}
	public function read($slug=null)
	{
		$data['menu_homepage'] = $this->getMenuHomepage();
    
		// get last article, batas 3
    	$this->load->model('product_support');
		$detail = Product_support::
			select(
				$this->global_lang."name".($this->global_lang!=''?' as name':''),
				$this->global_lang."description".($this->global_lang!=''?' as description':''),
				"created_at",
				"image"
			)
			->where("slug",$slug)
			->first();

		if (count($detail)==0) {
			// slug menu tidak valid, tampilkan halaman 404

			show_404();
			die();
		}

		$data['title'] 	= $detail->name;
		$data['other_info'] = Product_support::
			select(
				$this->global_lang."name".($this->global_lang!=''?' as name':''),
				"slug",
				"created_at",
				"image"
			)
			->orderBy("created_at","desc")
			->limit(6)
			->get();
		$data['detail'] = $detail;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/product_support_detail', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	
	}

}

/* End of file Blogs.php */
/* Location: ./application/controllers/Blogs.php */