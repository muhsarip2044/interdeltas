<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Managements extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
		/*
		* Penampil sub
		*/
	public function index()
		{
		$data['menu_homepage'] = $this->getMenuHomepage();
   
		$data['title'] 	= 'Komisaris dan Direksi';

		$this->load->model('management');
		$data['komisaris'] = Management::where("type",1)->orderBy("sort","asc")->get();
		$data['direksi'] = Management::where("type",2)->orderBy("sort","asc")->get();

		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/management', $data, true);
		$this->load->view($this->folder_view_frontend.'master_blog',$data);	

	}

}

/* End of file Managements.php */
/* Location: ./application/controllers/Managements.php */