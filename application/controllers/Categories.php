<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CLIENT_Controller {
	protected $limit=10;

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* Penampil sub
	*/
	public function index($slug_division=null,$slug_category=null)
	{


		$data_division = $this->get_header_division($slug_division);
		$data['header_navbar'] = $data_division['html'];

		$data['division'] = $data_division['data'];

		//Mengambil data category 
		$cat = Category_product::where("slug",$slug_category)->with("product")->first();

		if (count($cat)==0) {
			// slug category tidak valid, tampilkan halaman 404

			show_404();
			die();
		}	

		$data['cat'] = $cat;


		// Menampilkan product2 / divisi
	    $per_page = 9;
	    if (!empty($_GET['page'])) {
	    	$offset = $_GET['page'];
	    }else{
	    	$offset = 0;
	    }
	    $prod = Product::where("category_id",$cat->id)
	    	->orderBy("created_at","desc")
	    	->limit($per_page)
	    	->offset($offset);
	    if (!empty($this->input->get('filter'))) {
	    	$brands 		= htmlspecialchars($this->input->get('brands_id'));
	    	$option_specs 	= htmlspecialchars($this->input->get('specs_id'));
	    }

	    $data['products'] = $prod->get();

	    // Menampilkan paginasi
	    $this->load->library('pagination_helper');
	    $total_data = Product::where("category_id",$cat->id)
	    	->orderBy("created_at","desc")->count();
	    $this->pagination_helper->config($total_data,$per_page,'categories/'.$slug_division.'/'.$slug_category);
        $data['render_pagination'] = $this->pagination_helper->render();

        // Menampilkan filter
        $option_array = array();
        $options = $this->db
        	->select("*")
        	->where("category_id",$cat->id)
        	->get("tb_category_options");
        if ($options->num_rows() > 0) {
        	foreach ($options->result() as $key) {
        		// Ambil spec
        		$specs = $this->db->select("*")->where("option_id",$key->id)->get("tb_category_option_specs");
        		if ($specs->num_rows()>0) {
        			array_push($option_array,(object)array("option"=>$key->name,"spec"=>$specs->result()));
        		}
        	}
        }
        $data['options'] = $option_array;

        // Menampilkan Brand
        $data['brands'] = $this->db->Select("*")->where("category_id",$cat->id)->get("tb_brand_products");

        // Set url 
        $data['url'] = base_url()."categories/".$slug_division."/".$slug_category;

        // Set url division
        $data['url_division'] = base_url()."divisions/".$slug_division;

        // set category_id
        $data['category_id'] = $cat->id;

        // set limit getting product
        $data['limit'] = $this->limit;



		// init tag
		$this->load->model('tag');
		$data['tags'] = Tag::all();

		$data['title'] 	= $cat->name." - Divisi ".$data_division['data']->name;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/category', $data, true);
		$this->load->view($this->folder_view_frontend.'master_division',$data);	

	}

	/*
	* API GET PRODUCTS
	*/
	public function ajax_get_product($category_id=null,$offset=0,$brand_id=null,$spec_id=null){
		return $this->getProduct($offset,$category_id,$brand_id,$spec_id);
	}

	/*
	* FUnction get product
	* @param integer category_id
	* @param array integer brand
	* @param array integer spec
	*/
	public function getProduct($offset=0,$category_id=null,$brand_id=null,$spec_id=null)
	{
		$data = $this->db
			->select("
				tb_products.name, 
				tb_products.image, 
				tb_products.description , 
				tb_products.slug,
				tb_category_products.name as category,
				tb_divisions.slug as division
			")
			->join("tb_category_products","tb_category_products.id=tb_products.category_id")
			->join("tb_divisions","tb_products.division_id=tb_divisions.id");
		
		// Parameter category id
		if ($category_id != 0) {
			$data->where("tb_products.category_id",$category_id);
		}
		// Parameter Brand
		if ($brand_id != 0) {
			$brands = explode("-", $brand_id);
			$data->where_in("tb_products.brand_id",$brands);
		}

		// Parameter Spec
		if ($spec_id != 0) {
			$specs = explode("-", $spec_id);
			$data->join("tb_product_specs","tb_products.id=tb_product_specs.product_id","right");
			$data->where_in("tb_product_specs.spec_id",$specs);
		}

		$data->order_by("tb_products.created_at","desc");
		$data->group_by("tb_products.id");
		echo json_encode($data->get("tb_products",$this->limit,$offset)->result());
	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */