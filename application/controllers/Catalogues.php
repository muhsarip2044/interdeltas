<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogues extends CLIENT_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* Penampil sub
	*/
	public function detail($slug_division=null,$id_catalogue=null)
	{
		// Menghasilkan template sesuai divisi
		$data_division = $this->get_header_division($slug_division);
		$data['header_navbar'] = $data_division['html'];

		$data['division'] = $data_division['data'];

		// Mencari ID menu
		$this->load->model('catalogue');
		$catalogue = Catalogue::where("id",$id_catalogue)->first();

		if (count($catalogue)==0) {
			// id product tidak valid, tampilkan halaman 404

			show_404();
			die();
		}	
		$data['catalogue'] = $catalogue;


		// init slide show

		$data['title'] 	= $catalogue->name;
		$data['content'] 	= $this->load->view($this->folder_view_frontend.'page/catalogue', $data, true);
		$this->load->view($this->folder_view_frontend.'master_division',$data);	

	}

}

/* End of file Catalogues.php */
/* Location: ./application/controllers/Catalogues.php */