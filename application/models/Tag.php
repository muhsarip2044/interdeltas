<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Tag extends Eloquent{

    protected $table = 'tb_tags';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'tags/'.$this->id;
    }

}
