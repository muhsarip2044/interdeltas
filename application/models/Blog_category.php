<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Blog_Category extends Eloquent{

    protected $table = 'tb_category_blogs';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'blogs/category/'.$this->id;
    }

    public function blog()
	{
		return $this->hasMany('Blog', 'category_id', 'id');
	}

}
