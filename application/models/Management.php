<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Management extends Eloquent{

    protected $table = 'tb_managements';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_path'];

    /**
     * Get the image path
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return 'assets/frontend/images/managements/';
    }
}
