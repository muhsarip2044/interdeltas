<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Finance_report extends Eloquent{

    protected $table = 'tb_finance_reports';
    public function quartal()
	{
		return $this->hasMany('Finance_quartal', 'finance_id', 'id');
	}

}
