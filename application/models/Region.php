<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Region extends Eloquent{

    protected $table = 'tb_regions';
    public function outlet()
	{
		return $this->hasMany('kodak_express', 'region_id', 'id');
	}

}
