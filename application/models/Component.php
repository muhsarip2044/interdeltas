<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Component extends Eloquent{

    protected $table = 'tb_components';

}
