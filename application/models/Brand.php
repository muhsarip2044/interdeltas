<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Brand extends Eloquent{

    protected $table = 'tb_brand_products';
    public function product()
    {
        return $this->hasMany('Product', 'brand_id', 'id');
    }
}
