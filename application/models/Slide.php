<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Slide extends Eloquent{

    protected $table = 'tb_slides';

    public function division()
    {
    	return $this->belongsTo("Division","division_id","id");
    }

}
