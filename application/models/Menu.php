<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Menu extends Eloquent{

    protected $table = 'tb_menus';

    public function parent()
    {
        return $this->belongsTo('Menu', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('Menu', 'parent_id');
    }

    public function division()
    {
        return $this->belongsTo('Division', 'division_id','id');
    }

}
