<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Finance_quartal extends Eloquent{

    protected $table = 'tb_finance_quartals';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['file_path'];

    /**
     * Get the file path
     *
     * @return string
     */
    public function getFilePathAttribute()
    {
        return 'assets/frontend/files/reports/';
    }
    public function finance()
    {
    	return $this->belongsTo("Finance_report","finance_id","id");
    }

}
