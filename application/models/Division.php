<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Division extends Eloquent{

    protected $table = 'tb_divisions';
    public function category()
	{
		return $this->hasMany('Category_product', 'division_id', 'id');
	}
	public function product()
	{
		return $this->hasMany('Product', 'division_id', 'id');
	}
	public function feature()
	{
		return $this->hasMany('Product_feature', 'division_id', 'id');
	}
	public function catalogue()
	{
		return $this->hasMany('Catalogue', 'division_id', 'id');
	}

}
