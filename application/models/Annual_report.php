<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Annual_report extends Eloquent{

    protected $table = 'tb_annual_reports';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['file_path'];

    /**
     * Get the file path
     *
     * @return string
     */
    public function getFilePathAttribute()
    {
        return 'assets/frontend/files/reports/';
    }

} 
