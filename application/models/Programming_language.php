<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Alsofronie\Uuid\Uuid32ModelTrait;

class Programming_language extends Eloquent{

	use Uuid32ModelTrait;
	private static $uuidOptimization = true;
	protected $primaryKey = 'id';
    protected $table = 'programming_languages';

}
