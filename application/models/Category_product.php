<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Category_product extends Eloquent{

    protected $table = 'tb_category_products';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'categories/'.$this->division->slug."/".$this->slug;
    }

    public function product()
	{
		return $this->hasMany('Product', 'category_id', 'id');
	}
    public function division()
    {
        return $this->belongsTo('Division', 'division_id','id');
    }

}
