<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Admin_menu extends Eloquent{

    protected $table = 'menu';
    protected $primaryKey = "id_menu";
    public function parent()
    {
        return $this->belongsTo('Admin_menu', 'level_one');
    }

    public function children()
    {
        return $this->hasMany('Admin_menu', 'level_one');
    }

}
