<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Product_support extends Eloquent{

    protected $table = 'tb_product_supports';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix','image_path'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'product_supports/read/'.$this->slug;
    }

    /**
     * Get the image path
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return 'assets/frontend/images/supports/';
    }

}
