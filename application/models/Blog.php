<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Blog extends Eloquent{

    protected $table = 'tb_blogs';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix','image_path'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'blogs/read/'.$this->slug;
    }

    /**
     * Get the image path
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return 'assets/frontend/images/blogs/';
    }

    public function category()
    {
    	return $this->belongsTo("Blog_category","category_id","id");
    }

}
