<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Product extends Eloquent{

    protected $table = 'tb_products';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix','image_path','video_path'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'products/'.$this->division->slug.'/'.$this->slug;
    }

    /**
     * Get the image path
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return 'assets/frontend/images/products/';
    }
    /**
     * Get the image path
     *
     * @return string
     */
    public function getVideoPathAttribute()
    {
        return 'assets/frontend/videos/products/';
    }

    public function division()
    {
        return $this->belongsTo('Division', 'division_id','id');
    }
    public function category()
    {
        return $this->belongsTo('Category_product', 'category_id','id');
    }

    public function tags()
    {
        return $this->hasMany("Product_tag","product_id","id");
    }
    public function brand()
    {
        return $this->belongsTo('Brand', 'brand_id','id');
    }
}
