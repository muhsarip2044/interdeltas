<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Message extends Eloquent{

    protected $table = 'tb_messages';
    public function category()
    {
        return $this->belongsTo('Division', 'division_id','id');
    }

}
