<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Product_feature extends Eloquent{

    protected $table = 'tb_product_features';
    public function product()
	{
		return $this->hasOne('Product', 'id', 'product_id');
	}

}
