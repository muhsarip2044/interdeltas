<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Product_tag extends Eloquent{

    protected $table = 'tb_product_tags';

    public function tag()
    {
        return $this->belongsTo('Tag', 'tag_id','id');
    }

}
