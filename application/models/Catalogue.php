<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Catalogue extends Eloquent{

    protected $table = 'tb_catalogues';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_prefix','image_path'];

    /**
     * Get the url prefix.
     *
     * @return string
     */
    public function getUrlPrefixAttribute()
    {
        return 'catalogues/'.$this->division->slug.'/'.$this->id;
    }
    /**
     * Get the path of image
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return 'assets/frontend/images/catalogues/';
    }

    public function division()
    {
    	return $this->belongsTo("Division","division_id","id");
    }

}
